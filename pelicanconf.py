#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = 'William Kramps'
SITENAME = 'William Kramps'
SITEDESCRIPTION = 'Tueur de bouchers à Clermont Ferrand'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Athens'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

PLUGIN_PATHS = ['plugins']
PLUGINS = ['jinja2content']
STATIC_EXCLUDES = ['includes']
STATIC_PATHS = ['data','js','css','images','html']
ARTICLE_EXCLUDES = ['data','js','css','images','html']
PAGE_EXCLUDES = ['data','js','css','images','html']

THEME = os.path.dirname(os.path.abspath(__file__)) + "/themes/brutalist"
FIRST_NAME = "William Kramps"
SITEIMAGE = 'site-cover.jpg'
## used for OG tags and Twitter Card data of index page

## path to favicon
FAVICON = 'pelly.png'
## path to logo for nav menu (optional)
LOGO = 'william_kramps.png'
DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = False
## first name for nav menu if logo isn't provided
FIRST_NAME = 'William Kramps'
## google analytics (fake code commented out)
GOOGLE_ANALYTICS = 'UA-155184647-1'
## Twitter username for Twitter Card data
TWITTER_USERNAME = 'William_Kramps'
## Toggle display of theme attribution in the footer (scroll down and see)
## Attribution is appreciated but totally fine to turn off!
ATTRIBUTION = False
## Add a link to the tags page to the menu
## Other links can be added following the same tuple pattern 
MENUITEMS = [] #[('tags', '/tags')]
## Social icons for footer
## Set these to whatever your unique public URL is for that platform
## I've left mine here as a example

TWITTER = 'https://twitter.com/William_Kramps'
FACEBOOK = 'https://www.facebook.com/william.kramps.63'
GITLAB = 'https://gitlab.com/WilliamKramps/williamkramps.gitlab.io/commits/master/'
REDDIT = 'https://www.reddit.com/r/clermontfd'
DIIGO = 'https://www.diigo.com/user/williamkramps'
PROTONMAIL = 'william.kramps@protonmail.com'

## Disqus Sitename for comments on posts
## Commenting mine out for this theme site
# DISQUS_SITENAME = 'brutalistpelican'
## Gravatar
## Commenting mine out so you can see how the theme looks without one
## See https://mamcmanus.com to see what it looks like with a Gravatar
# GRAVATAR = 'https://www.gravatar.com/avatar/a5544bcae63c5d56c0b7a3fa0ab5b295?s=256'