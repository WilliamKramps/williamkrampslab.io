Title: Effondrement
Slug: effondrement

## Avertissements ...

Soyez conscient que, selon le degré de conscience que vous avez du problème de l'effondrement, ce que vous allez lire peut être extrêmement anxiogène et influer sur votre vie de manière immédiate. 

Prenez le temps de tout lire et surtout prenez du temps pour digérer ce que vous lirez.

## L'effondrement c'est quoi?

"La [collapsologie](https://fr.wikipedia.org/wiki/Collapsologie), ou science de l'effondrement théorise qu'une conjugaison de problémes économiques, sociaux, d'accés aux ressources, de dérèglements climatiques pourraient amener à une déstabilisation durable de nos sociétés dites développées."

<em style="float: right">Wikipédia</em>

[Voici une vidéo qui présente très succintement la problématique de l'effondrement](https://www.youtube.com/watch?v=_hhCNq5x8kY). 

[La conférence de Vincent Mignerot est bien plus détaillée, je vous la recommande pour approfondir](https://www.youtube.com/watch?v=9ME2gHHEdH8).

En gros il y a  [12 effets ciseaux qui nous guettent](https://www.collaborativepeople.fr/single-post/2019/11/29/Economie-Social-Ressources-Climat-12-effets-ciseaux-qui-nous-guettent) et qui sont tous liés entre eux, c'est à dire que la réalisation de l'un de ces effets ciseaux déclenchera mécaniquement la réalisation d'autres effets ciseaux. On appelle ça l'[effet domino](https://www.facebook.com/notes/jean-marc-jancovici/selon-une-%C3%A9tude-les-risques-deffet-domino-des-points-de-basculement-sont-plus-im/10156284575338191/).

[![12 effets ciseaux qui nous guettent]({attach}/images/12-ciseaux.jpg)](https://www.collaborativepeople.fr/single-post/2019/11/29/Economie-Social-Ressources-Climat-12-effets-ciseaux-qui-nous-guettent)

L'actualité regorge d'alertes, qui viennent conforter l'idée d'un effet dominos entre ces effets ciseaux, aboutissant à un effondrement de notre société thermo industrielle.

* [La planète va tellement mal qu'il n'y a plus une minute à perdre, avertit l'ONU](https://www.rts.ch/info/monde/10895642-la-planete-va-tellement-mal-qu-il-n-y-a-plus-une-minute-a-perdre-avertit-l-onu.html)

* ["Je ne vois pas comment nous pourrions échapper à une nouvelle crise financière" : entretien avec Gaël Giraud sur les années 2020](https://www.marianne.net/economie/je-ne-vois-pas-comment-nous-pourrions-echapper-une-nouvelle-crise-financiere-entretien-avec)

* [Jean-Marc Jancovici évoque la décennie 2020 : "Pour résoudre le problème écologique, il faudra baisser notre pouvoir d'achat"](https://www.marianne.net/politique/jean-marc-jancovici-evoque-la-decennie-2020-pour-resoudre-le-probleme-ecologique-il-faudra)

Je vous passe les tensions internationales, les différents mouvements populaires qui éclatent aux 4 coins de la planère, l'épuisement des ressources, l'Australie qui brûle ... 

Une liste qui paraît hélas plus destinée à s'allonger qu'à se résorber.

Si vous voulez plus de détails, je vous invite à suivre

- [Pablo Servigne](https://www.youtube.com/results?search_query=pablo+servigne) pour la théorie de l'effondrement en général
- [Jean-Marc Jancovici](https://www.youtube.com/channel/UCNovJemYKcdKt7PDdptJZfQ) pour les questions d'énergie
- [Aurélien Barrau](https://www.youtube.com/results?search_query=aurelien+barrau)
- [Vincent Mignerot](https://www.youtube.com/results?search_query=vincent+mignerot)
- [Philippe Bihouix](https://www.youtube.com/results?search_query=bihouix) pour les low tech
- [Gael Giraud](https://www.youtube.com/results?search_query=gael+giraud) pour la crise économique et financière
- [Clément MONTFORT - Web-Séries Documentaires](https://www.youtube.com/channel/UC0i7t1CC7T0xheeBahaWZYQhttps://www.youtube.com/channel/UC0i7t1CC7T0xheeBahaWZYQ)
- *ceux que vous me suggèrerez ...*

**Une fois que ce constat est fait, il faut tout d'abord le [digérer](http://www.tourdefrancedesalternatives.fr/focus-debats/collapsologie-entre-espoir-lucidite/)**, car si vous avez bien compris de quoi on parle, et que vous n'étiez pas familier avec l'idée de l'effondrement, vous derviez passer par une phase de déprime. C'est normal, nous sommes tous passés par là. 

Une fois le choc passé, la seule question qui se pose est: "Qu'est qu'on fait?"

Une réponse est créons de s"[la résilience](/pages/resilience.html)".

**N.B.** si vous n'êtes pas à l'aise avec les concepts de la [collapsologie](https://fr.wikipedia.org/wiki/Collapsologie) mais que vous pensez tout de même que nous devrions apporter **plus de soin à notre environnement naturel**, dites vous qu'anticiper l'[effondrement](/pages/effondrement.html), va dans le même sens qu'une **transition vers une humanité plus respectueuse de la planéte qui l'héberge**, et que **nous visons des objectifs similaires vous et moi**.

