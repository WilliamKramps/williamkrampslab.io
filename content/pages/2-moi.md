Title: Moi
Slug: moi


<div align="center" style="font-size: 50px"> {% import 'includes/wk.j2' as wk %} {{ wk.social() }} </div>

## Pour faire court

Il y a quelques années,  j'ai lu [Comment tout peut s'effondrer ; petit manuel de collapsologie à l'usage des générations présentes](http://www.librairielesvolcans.com/9782021223316-comment-tout-peut-s-effondrer-petit-manuel-de-collapsologie-a-l-usage-des-generations-presentes-pablo-servigne-raphael-stevens/). S'en est suivi une prise de conscience de la gravité de la situation, une phase de déprime profonde, suivie d'une conviction forte: 

**"Pour ne pas tomber de trop haut: nous devons être résilient, ici et maintenant"**

L'échelle locale est la seule échelle permettant de créer de la [résilience](/pages/resilience.html) au niveau du territoire que nous habitons, afin de gérer, ou plutôt de minimiser l'impact d'un [effondrement](/pages/effondrement.html) probable à horizon d'une dizane d'années. En mandats municipaux ce délais représente de 1 à 2 mandats.

Je me suis donc intéressé donc de près à la campagne des <strong>élections Municipales et communautaires</strong> des 15 et 22 mars 2020 à <strong>[Clermont Ferrand](https://clermont-ferrand.fr/) / [Clermont Métropole](https://www.clermontmetropole.eu/ma-metropole/la-carte-et-le-territoire/que-fait-la-metropole/)</strong>, car je souhaitais voir les sujets de l'[effondrement](/pages/effondrement.html) et de la [résilience](/pages/resilience.html) passer dans les priorités de nos candidats.

**N.B.** si vous n'êtes pas à l'aise avec les concepts de la [collapsologie](https://fr.wikipedia.org/wiki/Collapsologie) mais que vous pensez tout de même que nous devrions apporter **plus de soin à notre environnement naturel**, dites vous qu'anticiper l'[effondrement](/pages/effondrement.html), va dans le même sens qu'une **transition vers une humanité plus respectueuse de la planéte qui l'héberge**, et que **nous visons des objectifs similaires vous et moi**.

## Je suis Clermontois ...

... depuis plusieurs dizaines d'années, et je dois préciser, pour être honnête, que je suis un administré moyennement cultivé sur sa commune: je ne suivais pas les [conseils municipaux même si je sais où les trouver](https://clermont-ferrand.fr/le-conseil-municipal-en-video) et je ne lisais qu'épisodiquement les titres et éventuellement quelques articles de [@La_Montagne](https://twitter.com/lamontagne_fr). Mais depuis depuis les élections je suis beaucoup plus assidu ;)

J'ai une vie dans la cité, comme j'imagine vous avez la vôtre. Sûrement vivons des choses communes, surement vivez vous des choses dont je n'ai pas idée. Les retours de **tous les clermontois** m'intéressent. j'essaie de considérer, et de présenter objectivement tous les points de vue.

*Tant que nous en sommes à ma vie dans la cité je tiens à préciser que je ne tue plus de boucher!*

## Je suis sur présent sur l'Internet

Je discute donc avec **tout le monde** sur [facebook](https://www.facebook.com/william.kramps.63), [twitter](https://twitter.com/William_Kramps).

Je suis aussi joignable par [mail](mailto:william.kramps@protonmail.com)

Je vais essayer de pousser régulièrement des nouvelles questions sur [le filet "clermontfd" de reddit](https://www.reddit.com/r/clermontfd) car je trouve que c'est une bonne plateforme pour débattre et faire remonter les commentaires intéressants.

Quand je récupère des infos, ou mieux des données qui permettent d'invalider ou de consolider un argument, je les mets sur mon [diigo](https://www.diigo.com/user/williamkramps).

J'ai aussi monté ce [blog](https://williamkramps.gitlab.io/) afin de 
- rassembler mes notes sur les élections municipales, et sur le mandat qui vient
- [donner la parole à ceux qui pourraient nous faire part de leurs expertises ou tout simplement de leurs avis](/pages/appel-a-contributions.html).
- permettre à tous de [me contacter](mailto:william.kramps@protonmail.com) pour rectifier / compléter mes notes

Le but est d'essayer de rendre plus lisibles les différentes controverses.

Je cherche à porter les problèmatiques transverses de l'[effondrement](/pages/effondrement.html) et de la [résilience](/pages/resilience.html) à l'attention de nos candidats aux municipales 2020 à Clermont Ferrand et sa métropole.

Si vous pensez que c'est important **interpellez les élus**, **partagez mes status**, **suivez l'actualité que je publie**, **soumettez moi la vôtre** et ensemble essayons de peser dans le débat pour travailler à la [résilience](/pages/resilience.html) de [Clermont Ferrand](https://clermont-ferrand.fr/) et de sa [Métropole](https://www.clermontmetropole.eu/ma-metropole/la-carte-et-le-territoire/que-fait-la-metropole/).

## Je suis agnostique et apolitique

Notez bien qu'[agnostique](https://fr.wikipedia.org/wiki/Agnosticisme) ne signifie pas [athé](https://fr.wikipedia.org/wiki/Ath%C3%A9isme).

Je ne suis et n'ai jamais été encarté nulle part, je n'ai jamais ne serait ce que milité pour aucun parti ... 

Je suis compatible avec **tout** même si j'ai quelques convictions ... 

Je crois notamment que:

- les mots ont un sens et que respecter le sens des mots, c'est respecter ses interlocuteurs
- l'utilisation du [pseudonymat](/pseudonymat.html) permet une plus grande liberté de ton dans le débat démocratique
- la transparence des élus envers leurs administrés est une nécessité pour la confiance et le repsect entre équipe municipale et administrés
- [l'effondrement est un scénario plus que probable il est inéluctable](https://www.youtube.com/watch?v=kLzNPEjHHb8)
- la technologie ne résoudra pas les problèmes qu'elle a posé. un corolaire serait que les solutions ne sont ni dans l'intelligence articfielle, ni dans le [séquestre artificielle de CO2](https://fr.wikipedia.org/wiki/S%C3%A9questration_du_dioxyde_de_carbone#S%C3%A9questration_artificielle), ni dans le recyclage des déchets ou tout autre intiative invalidée par l'[effet rebond](https://fr.wikipedia.org/wiki/Effet_rebond_(%C3%A9conomie)). 
- De même dire que "la transition énergétique / écologique est une chance" ou un "moteur de croissance verte" est une absurdité
- l'énergie la plus propre est celle que l'on utilise pas
- les problèmatiques d'[effondrement](/pages/effondrement.html) et de [résilience](/pages/resilience.html) n'ont rien à voir avec la morale et tout à voir avec le pragmatisme.
- les policiers municipaux, les pompiers et les impôts ont le même problème: trop de gens ne comprennent pas leur utilité
- sécuriser coûte toujours en ressource et en énergie. Passer d'un monde où les ressources et l'énergie sont abondantes, à un monde où elles sont rares nécessite de repenser ce qui a besoin d'être sécurisé
- les [communs](https://fr.wikipedia.org/wiki/Communs) sont une opportunité pour la [résilience](/pages/resilience.html). Notamment en terme de sécurité.
- un service publique n'est pas rentable par essence, puisqu'il est sensé rendre service à tous, même à ceux qui n'ont rien pour se l'offir
- j'ai plus de pouvoir en choisissant comment je dépense mes euros qu'en votant aux présidentielles
- gérer le risque est une nécessité! Mais n'oublions jamais que "vivre tue"


## William Kramps

Vous me connaissez en tant que personnage de [Drôle de drame ](https://fr.wikipedia.org/wiki/Dr%C3%B4le_de_drame) un film de Marcel Carné de 1937, adaptation de Prévert.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9RRb-6L7ix4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Vous pouvez aussi me connaître par [Ludwig Von 88](https://fr.wikipedia.org/wiki/Ludwig_von_88) qui m'a dédié une chanson

<iframe width="560" height="315" src="https://www.youtube.com/embed/GrxA23DYyyE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Vous n'en saurez pas plus sur mon intimité de tueur de bouchers pour des raisons évidentes de sécurité ... (même si je le rappelle je ne tue plus de bouchers! C'est important de le préciser!)