Title: Appel à contributions
Slug: appel-a-contributions


Toutes les contributions, suggestions, corrections sont les bienvenues pour éclaircir un point ou apporter la contradiction. 

Dans une démarche de transaprence, je publierai sur ce [blog](https://williamkramps.gitlab.io/) tout ce que l'on m'enverra, sous la seule conditions:

1 - que le texte ait un rapport direct avec la [ville de Clermont Ferrand](https://clermont-ferrand.fr/) et / ou [Clermont Auvergne Métropole](https://en.wikipedia.org/wiki/Clermont_Auvergne_M%C3%A9tropole)

2 - que les éléments avancés soient sourcés, c'est à dire qu'il est à la charge du contributeur de présenter un ensemble de liens ou de références permettant de **vérifier le bien-fondé et la bonne foi de l'argumentaire qu'il développe**.

Vous l'aurez compris vous pouvez publier ici sous votre vraie identité ou [sous pseudonymat](pseudonymat.html).

**Les candiat.e.s et leurs (ex-)soutiens sont également les bienvenu.e.s.**

## Les thématiques qui me semblent à discuter pour améliorer la [résilience](/pages/resilience.html) de notre territoire

* résilience alimentaire

* résilience hydrique

* résilience sociale

* résilence énergétique

* résilience écologique 

* résilience climatique

* résilience organisationnelle

n'hésitez pas à me suggérer de nouveaux thèmes ou des noms, regroupements plus pertinents.

[me contacter](mailto:williamkramps@protonmail.com)