Title: Municipales 2020
Slug: municipales2020

J'ai fait un travail de recensement pour Clermont Ferrand uniquement. Si vous avez fait ou voulez faire ce travail pour les autres commmunes de la métropole, n'éhsitez pas à me les envoyer pour que je les publie ici.

## Les listes

## Clermont Ferrand

**Cause Commune** (Phil Fasquel)

[<i class="fab fa-facebook"></i>](https://www.facebook.com/CauseCommuneClermont/) /
[<i class="fab fa-twitter"></i>](https://twitter.com/cause_commune)

Liste sans étiquette politique

---

**Changer Clermont** (Eric Faidy)

[<i class="fas fa-globe-europe"></i>](https://ericfaidy.fr/) /
[<i class="fab fa-twitter"></i>](https://twitter.com/EricFaidy) /
[<i class="fab fa-facebook"></i>](https://www.facebook.com/EricFaidy.cl.fd/)
[<i class="fab fa-facebook"></i>](https://www.facebook.com/ClermontFd2020)

Ensemble, transformons Clermont-Ferrand
@ClermontFd2020

LREM

---

**Clermont en Commun** (Marianne Maximi)

[<i class="fas fa-globe-europe"></i>](https://clermontencommun.fr) /
[<i class="fab fa-twitter"></i>](https://twitter.com/ClermontCommun) /
[<i class="fab fa-facebook"></i>](https://www.facebook.com/clermontencommun/)

LFI + le collectif "Nous Sommes Prêt.e.s" (c.f. chronologie ci-après)

---

**"L'essentiel pour Clermont"** (Anne Biscos) 

[<i class="fab fa-facebook"></i>](https://www.facebook.com/annebiscos) /
[<i class="fab fa-twitter"></i>](https://twitter.com/AnneBiscos)

RN

---

**Naturellement Clermont** (Olivier Bianchi)

[<i class="fas fa-globe-europe"></i>](https://olivierbianchi2020.fr) / 
[<i class="fab fa-twitter"></i>](https://twitter.com/bianchi2020) /
[<i class="fab fa-facebook"></i>](https://www.facebook.com/bianchi2020/)

PS + PCF + Génération-s + EELV

---

**Révéler Clermont** (Jean-Pierre Brenas)

[<i class="fas fa-globe-europe"></i>](https://www.jeanpierrebrenas.com/) /
[<i class="fab fa-twitter"></i>](https://twitter.com/jpbrenas) /
[<i class="fab fa-facebook"></i>](https://www.facebook.com/jpbrenas/)

LR + Modem

---


Si vous avez une liste exhaustive des candidats dans les autres communes de la métropole, envoyez les moi je complèterai

## Chronologie à Clermont Ferrand

- le 13 juin 2018 [Olivier Bianchi PS (Partic Socialiste) s'est déclaré candidat à sa propre succession](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/municipales-2020-a-clermont-ferrand-olivier-bianchi-est-le-premier-a-se-lancer_12882460/)

- en juin 2019 le [député Michel Fanget (MoDem) est candidat à la mairie de Clermont-Ferrand](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/municipales-le-depute-michel-fanget-modem-candidat-a-la-mairie-de-clermont-ferrand_13575802/)

- le 1er juillet [Eric Faidy est investi tête de liste de LREM (La République En Marche pour les municipales 2020)](https://www.francebleu.fr/infos/politique/clermont-ferrand-eric-faidy-tete-de-liste-de-la-republique-en-marche-pour-les-municipales-2020-1562017342): la candidature de Michel Fanget devient une légende urbaine visiblement ...

- le 19 septembre [une tribune pour alerter face à l’urgence climatique est publiée dans la montagne](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/municipales-a-clermont-ferrand-une-tribune-pour-alerter-face-a-lurgence-climatique_13645155) par [Nous Sommes Prêt.e.s](https://www.facebook.com/noussommespret.e.s.clfd/), un collectif de 130 chercheurs, militants, paysans, salariés, citoyens engagés dans les luttes sociales, les mouvements climatiques et écologiques. une lettre ouverte est envoyée dans la foulée aux "forces politiques de gauche écologistes de Clermont Ferrand et sa bio région". EELV et LFI sont les seuls partis à manifester leur intérêt pour cette lettre ouverte.

- le 1er octobre [Jean Pierre Brenas se présente comme candidat LR (Les Républicains)](https://www.francebleu.fr/infos/politique/clermont-ferrand-jean-pierre-brenas-candidat-a-la-mairie-je-ne-me-suis-jamais-senti-aussi-pret-1569936049). Il était opposé à Bianchi au second tour en 2014.

- le 5 octobre [un accord est trouvé entre EELV (Europe Écologie - Les Verts) et le maire PS sortant Olivier Bianchi](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/municipales-a-clermont-ferrand-un-accord-trouve-entre-europe-ecologie-les-verts-et-le-maire-ps-sortant-olivier-bianchi_13657002/)

- le 19 novembre [Marianne Maximi est désignée tête de liste de « Clermont-Ferrand en Commun », soutenue par La France insoumise](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/municipales-marianne-maximi-designee-tete-de-liste-de-clermont-ferrand-en-commun-soutenue-par-la-france-insoumise_13695784/) et Nous Sommes prêt.e.s

- le 26 novembre [le collectif citoyen Cause Commune veut constituer une autre offre politique](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/municipales-a-clermont-ferrand-le-collectif-citoyen-cause-commune-veut-constituer-une-autre-offre-politique_13694399/)

- le 30 décembre [la Montagne annonce la candidature de Michel Fanget, qui lancera sa campagne le 9 janvier avec comme slogan "L'important c'est Clermont Ferrand](https://www.facebook.com/photo.php?fbid=10157794925734561&set=a.10151263012419561&type=3&theater) je vous laisse lire l'analyse  de [Jean Nöel Delorme](https://www.facebook.com/jndelorme1) qui accompagne la photo 

- le 09 janvier [en lançant sa campagne, le MoDem Michel Fanget fera donc face au candidat LREM](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/municipales-a-clermont-ferrand-en-lancant-sa-campagne-le-modem-michel-fanget-fera-donc-face-au-candidat-lrem_13720784/)

- le 14 janvier [Marie Savre se présente pour Lutte ouvrière](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/municipales-a-clermont-ferrand-marie-savre-et-lutte-ouvriere-pour-faire-entendre-la-voix-des-travailleurs_13723190/)

Si vous avez une chronologie pour les autres communes de la métropole, envoyez les moi je complèterai