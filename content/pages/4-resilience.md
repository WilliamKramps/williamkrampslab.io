Title: Résilience
Slug: resilience


### On s'organise!

l'**anticipation** et la **coopération** sont, de l'avis de tout ceux qui se sont posés la question de l'"atterrissage en cas d'[effondrement](/pages/resilience.html)", les clés de la résilience

#### La résilience?

[La résilience](https://fr.wikipedia.org/wiki/R%C3%A9silience) c'est la capacité à surmonter un événement, à absorber les chocs et à revenir dans un état "normal". 

Gagner en résilience c'est penser dès maintenant une organisation sur un périmètre relativement petit, capable de "fonctionner" avec beaucoup moins d'énergie ... ce qui dans le contexte de notre société thermoindustrielle impacte environ tous les secteurs: nourriture, eau, santé, mobilité, communication, chauffage, économie, sécurité ... 

**... pour ne citer que les plus évidents!**

Il nous faut donc repenser dès maintenant nos modes de vie afin qu'ils soient le plus résilient possible, c'est à dire suffisament **simples et sobres** pour qu'en cas d'effondrement nos besoins fondamentaux - boire, manger, se chauffer, dormir - puissent être satisfaits. 


Notez que sur la forme et sur les objectfis, la résilience et l'urgence climatique (déclarée un peu partout dans le monde suite aux [nombreuses alertes des scientifiques](https://www.futura-sciences.com/planete/actualites/climatologie-11000-scientifiques-declarent-urgence-climatique-69220/)), se répondent complètement.

**Si vous ne croyez pas en un [effondrement](/pages/resilience.html) imminent, et que vous êtes en revanche persuadé qu'il faut baisser notre impact sur la planète, vous constaterez que nous visons vous et moi les mêmes objectifs**

#### A quelle échelle?

Dans un monde où l'énergie n'est plus gratuite, ou en tout cas elle est bien plus coûteuse, qu'à l'heure actuelle, il paraît logique de réfléchir à l'échelle locale. Cette vision permet notamment de réduire les déplacements .

C'est au niveau planétaire que l'interdépendance est une vulnérabilité. A un niveau plus local, c'est une force.

C'est exactement pour cette raison que [je m'intéresse de près aux élections municipales 2020](/pages/municipales2020.html): 

**je souhaiterais mettre la problématique de la résilience de notre territoire (ou de l'urgence climatique si vous préférez ce terme) au coeur de la campagne des élections municipales 2020**.


#### Quelques réflexions à partager

- la **technologie** est une fuite en avant: elle augmente notre impact sur le climat, car elle consomme toujours plus d'énergie et produit toujours plus de déchets, et elle ne nous sauvera pas des problèmes qu'elle a elle même créé.

- l'énergie la plus propre est celle que l'on ne consomme pas

- La résilience n'est pas spécialement morale, elle répond avant tout à une logique pragmatique: 
    
    - on peut être sensible au bien être animal ou pas, ce qui est sûr c'est qu'en terme d'impact écologique, [manger de la viande est plus polluant que manger des légumes](https://www.liberation.fr/checknews/2018/03/20/les-vegetaliens-et-vegans-ont-il-un-impact-environnemental-moins-important-que-ceux-qui-mangent-de-l_1653328).
    
    - on peut être sensible aux bien être des migrants ou pas, ce qui est sûr c'est qu'habiter à côté de bidonville aux conditions d'hygiène précaire peut affecter la santé de toute la ville
    
    - on peut être attaché à l'automobile mais on comprend rapidement que si l'[aire de la ville n'est plus respirable](https://france3-regions.francetvinfo.fr/auvergne-rhone-alpes/puy-de-dome/clermont-ferrand/puy-dome-pollution-taux-particules-fines-eleves-ce-debut-annee-1769121.html) les ballades en SUV vont devenir moins agréables.

    - ...

- Ce qui va être douloureux, c'est la descente. Pour minimiser la souffrance pendant la descente il faut absolument minimiser la violence des chocs et essayer de ne pas derscendre trop bas.

- ceux qui vivront le "mieux" en cas d'[effondrement](/pages/effondrement.html) sont ceux qui s'entraident le plus

- la résilience ne viendra pas d'en haut, c'est à nous de la réclamer et de la construire

- l'inégalité est un facteur aggravant pour l'exploitation des ressources. Si inégalité il y a, il y aura un coup fort pour protéger les ressources. Ce coup peut être évité si nous pensons dès maintenant en terme de **communs accessible à tous**.


#### Quelques ressources à partager

Je ne suis pas le seul à vouloir voir ces sujets aborder à l'occasion des élections municipales 2020, il existe déjà plusieurs initiatives qui vont dans ce sens, voir plusieurs municipalités qui ont déjà entamées leur chemin vers la [résilience](/pages/resilience.html).

* [SOS Maire, Autonomie et résilience des communes rurales](https://sosmaires.org/)

    * [Face à l'effondrement, si j'étais maire ? comment citoyens et élus peuvent préparer la résilience par Alexandre Boisson et Andre-jacques Holbecq](http://www.librairielesvolcans.com/9782364291324-face-a-effondrement-si-j-etais-maire-comment-citoyens-et-elus-peuvent-preparer-la-resilience-alexandre-boisson-andre-jacques-holbecq/)

* [Le pacte pour la transition](https://www.pacte-transition.org/) qui a déjà été signé par [Clermont Ferrand](https://alternatiba.eu/puy-de-dome/alternatives-territoriales/) et devrait être suivi par un [comité de suivi citoyen](mailto:63@alternatiba.eu)

* [Haut Comité Français pour la Résilience Nationale](https://www.hcfdc.org/)

    * [Le Pavillon Orange, label décerné aux communes qui répondent à des critères en termes de sauvegarde et de protection des populations face aux risques et menaces majeurs](https://www.pavillon-orange.org/)

* [Réseau d’entraide entre ruraux et citadins](https://entraide-humanum.org/)

* [Le petit manuel de résilience](https://www.youtube.com/channel/UCihaNBB1GDpNbB3g4TdvFSw)

* Quelques communes pionnières sur le sujet

* *N'hésitez pas à compléter cette liste*

#### Aux municipales 2020 à Clermont Ferrand / Métropoles

A mon avis les Municipales 2020 sont le bon moment pour:

- interpeler nos candidats
- s'assurer qu'ils ont bien compris les enjeux
- les faire s'engager sur des objectifs chiffrés permettant d'améliorer la résilience de la ville / métropole
- suivre l'accomplissement de ces objectifs tout au long du mandat

Ecrivez à vos candidats favoris, et regardez s'ils intègrent la [résilience](/pages/resilience.html) à leur programme.

Soutenez les associations qui interpellent les candidats et relayez les réponses.

### Quelles mesures?

Si vous vous demandez quels types d'objectifs proposer à nos candidat, voici un liste non exhaustive (que je vous invite comme pour le reste à compléter)

- faire figurer la sécurité alimentaire et hydrique dans le [DICRIM (Dossier d'Information Communal sur les RIsques Majeurs)](https://fr.calameo.com/read/000000209b0e8c2ea0a41) et avoir un vrai plan de crise.

- constituer une [Régie agricole communale](https://www.monmandatlocal.fr/developpement-durable/produire-local-bio-pari-ambitieux-regies-agricoles-communales/)

- constituer une [Régie énergétique communale](https://www.lavoixdunord.fr/595214/article/2019-06-08/la-ville-lance-son-plan-solaire-avec-l-aide-d-energethic-et-des-citoyens) 

- promouvoir un [plan solaire clermontois](https://plansolaire.loos-en-gohelle.fr/site/)

- promouvoir et communiquer sur [la Doume](https://doume.org) ou [duniter](https://duniter.org)

- promouvoir et communiquer sur les initiatives locales comme la [coop des dômes](http://www.coopdesdomes.fr/), (vous pouvez compléter cette liste)

- constituer une régie de la gestion des déchets

- faire des rénovations et notamment des rénovations énergétiques une priorité absolue

- promouvoir les transports en commun en investissant massivement dans un réseau métropolitain efficace. Promouvoir les mobilités durables. Le tout au détriment des voix de circulation automobiles.

