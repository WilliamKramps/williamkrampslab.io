Title: Pourquoi le pseudonymat?
Date: 2020-01-20
Category: municipales2020
Tags: démocratie, élection
Slug: pseudonymat


## comptes anonymes

Je ne suis pas réellement anonyme, dans la mesure où, Google, Facebook, Twitter, reddit, gitlab, et protonmail ont tout à fait la possibilité de m'identifer quand je me connecte à leurs services.

## faux comptes 

Je ne suis pas un faux, dans la mesure où je ne suis pas un robot, je ne suis pas plusieurs (si je le deviens je le préciserai), et je tente d'obtenir les informations les plus factuelles possibles.


## comptes sans tête

J'ai une tête: j'en suis plutôt content, merci!

## trolls (de parti politique)

Je ne suis pas non plus un troll de parti politique dans la mesure ou j'en ai après tous les partis, ils m'ont tous déçus à un moment ou à un autre. En revanche je n'en ai après personne. 

Beaucoup de trolls de parti politique parlent sous une "vraie" identité :D

Devant la mauvaise foi je peux devenir un troll tout court, mais ce ne sera pas pour faire le VRP.

## Pseudonymat ?

Ma pratique des réseau sociaux avec William Kramps s'appelle du pseudonymat, c'est une possibilité offerte par Internet, au même titre que la surveillance. 

Les deux doivent coexister pour que le média soit sûr.

Ne pas incarner une personne physique me donne une vraie liberté de ton et de parole, qui me semble indispensable pour mener le débat démocratique qui précède une élection et en particulier les municipales.

En effet si je ne suis pas sous pseudonyme:

* Comment critiquer le maire sortant si je suis employé à la mairie? Si je m'investis dans une association subventionnée d'une manière ou d'une autre par la mairie?
* Comment questionner le programme d'Eric Faidy si je suis employé chez Michelin?
* Comment me positionner contre les pesticides, les graines hybrides et pour la permaculture si je travaille chez Limagrain?
* Comment respecter mon droit de réserve si je suis fonctionnaire?
* Tout le monde peut il assumer un "j'aime" sur la page d'un candidat diamétralement opposé à ses idéaux?

Je ne crois pas que ce soit une question de courage ... Et quand bien même, les couards votent aussi et ont le droit de participer au débat!

## Pourquoi vous devriez faire pareil?

Je pense que nous avons tous une bonne raison de préserver notre identité pour **participer au débat**.

Le scrutin lui, est réellement anonyme, dans la mesure où on ne peut pas savoir qui a voté quoi.

D'après moi les seuls qui devraient être identifiables sont les candidats pour lesquels on vote et éventuellement leur équipe proche, le reste ne devrait tourner qu'autour des propositions de leur programme respectif.