Title: Le temps, le texte et la transparence
Date: 2020-03-09
Slug: temps-texte-transparence


## TLDR;

Les données publiques sont difficilement accessibles, c'est pourquoi j'ai décidé de publier ici quelques jours avant les élections deux jeux de données publiques, qui ne sont juste pas accessible aux administrés. 

Je les ai mis en forme pour que chacun puisse se faire une idée objective et je pense que ce travail devrait être généralisé au niveau de la métropole, au fil du mandat, parce que c'est la loi

- [Le Plan Pluriannuel d'Investissement](/programme-pluriannuel-investissement.html)
- [Les subventions versés aux associations](/subventions-aux-associations.html)

## Le temps

Le temps c'est le nerf de la guerre.

On a mis en place une économie du temps, on l'échange contre de l'argent et ça s'appelle le **travail**, on le délègue et ça s'appelle la **démocratie réprésentative**. 

En votant on choisit à qui on délègue **le temps qu'on ne passera pas à gérer les problèmes** du pays, dans le cas des présidentielles, ou de la cité, dans le cas des municipales.

Dit autrement nos élus, concentrent le capital temps de tous les citoyens ou administrés pour un temps donné.

Vu comme ça on comprend bien vite tous les problèmes que ce modèle réprésentatif peut poser:

- l'élu responsable du capital temps des citoyens, n'en reste pas moins soumis au même temps fini que les autres. On jugule le problème en lui permettant de se décharger sur des ministres au niveau nationale ou des adjoints au niveau de la cité. je dis "juguler" parce que ces délégués sont eux même soumis au même temps fini et concentrent un capital colossal de temps délégué par les citoyens.

- cette économie de temps est complètement transposable à l'économie dite réelle (en euros quoi), qui est soumise à la maximisation des profits: on attend de manière symétrique une rentabilité optimale du temps délégué aux élus

On comprend aisément que bon nombres de citoyen peuvent se sentir éloignés de ce système soit pour des raisons philosophiques soit pour des raisons plus pragmatiques comme se sentir exclu du partage des profits. Ces citoyens vont enrichir **les rangs de l'abstention** ...

## Le texte

Dans tous les cas on ne peut pas prendre des décisions qui conviennent à tout le monde, c'est une évidence. 
Du coup les élus cherchent (ou sont sensés chercher) **l'intérêt général**.

Nos élus participent à des commissions, prennent des décisions, élaborent des textes qui sont sensés servir l'intérêt général et qui permettront de prendre des décisions systématiques en les appliquant.

A ce niveau là de mon propos, je vais laisser tomber le niveau nationale, pour mieux me concentrer sur la cité, avec un exemple qui a été très discuté pendant la campagne: **[Plan Local d'Urbanisation](https://fr.wikipedia.org/wiki/Plan_local_d%27urbanisme)** aka **le [PLU](https://clermont-ferrand.fr/plu)**.

Le PLU est typiquement un texte, sensé servir l'intérêt général des habitants de la cité.

Saviez vous que le PLU actuel, voté en 2016, a fait l'objet d'[une enquête publique](https://clermont-ferrand.fr/plu)? On ne peut pas dire que ce texte a été fait dans notre dos, n'est ce pas?

Mais **qui a du temps** et les compétences pour essayer de se projeter dans ce que peut impliquer un PLU? Les **gens du métiers (archi, BTP, etc ...)** c'est sûr! pour le reste on peut s'interroger sur l'**efficience démocratique** d'une telle consultation ....

D'ailleurs malgré cette consultation, à chaque nouvelle construction, on s'interroge:

- les riverains sont rarement enchantés de l'arrivée d'un nouveau projet immobilier
- les gens qui se sont vus refusés un permis de construire gardent en général une certaine frustration ...
- nous sommes de plus en plus d'administrés à penser qu'on devrait [faire très attention à ce qu'il nous reste de nature](https://ideas4development.org/solutions-fondees-sur-la-nature-biodiversite-sauve-climat/?fbclid=IwAR029phzPvOLaejIVXv_nKWEqENv0-4cAb6t4ETOWQhyco3F9PpjnP74Nn4)

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Malgré une pétition et un recours devant le tribunal, les promoteurs sans scrupules bien aidés par une municipalité &quot;complaisante&quot; ont, sans attendre les recours, débuté l&#39;arrachage des arbres. <br>Sans prévenir et en bloquant les habitants avec leurs engins.<a href="https://twitter.com/hashtag/ClermontFerrand?src=hash&amp;ref_src=twsrc%5Etfw">#ClermontFerrand</a> <a href="https://t.co/IqE5MXRbj7">pic.twitter.com/IqE5MXRbj7</a></p>&mdash; Charles BE-NT (@Charles_BE1) <a href="https://twitter.com/Charles_BE1/status/1227929492968083458?ref_src=twsrc%5Etfw">February 13, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

Dans un cas comme celui ci, on a arraché quelques **arbres centenaires** pour y mettre: **une construction**!

L'adjoint à l'urbanisme s'est déplacé, car les riverains n'étaient pas contents. Il leur a dit que la construction était **conforme au PLU** et que **la parcelle n'avait pas été signalée il y a 4 ans** lors de la consultation pour le PLU (quoi y a vait une consultation??).

Les cas comme celui là sont nombreux: quartier St Jacques, rue Thévenot Thibaud, ...

## La transparence

Peut on à l'heure de l'urgence climatique considérer qu'un PLU qui dure 6 ans et plus, sans bouger, est viable?

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="fr" dir="ltr">Et finalement ce PLU pourquoi n&#39;y écrit on pas que l&#39;on arrachera plus d&#39;arbres à partir de maintenant ca éviterait ce genre de situation non?<br> <a href="https://t.co/fmXxoSbUm9">https://t.co/fmXxoSbUm9</a><br><br>Qu&#39;est ce qui empêche de le faire?<br>Ca donnerait du sens a &quot;la ville se reconstruit sur elle même&quot; non?</p>&mdash; William Kramps (@William_Kramps) <a href="https://twitter.com/William_Kramps/status/1235834880216334337?ref_src=twsrc%5Etfw">March 6, 2020</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Peut on le faire évoluer?

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="fr" dir="ltr">Ou proposer un Malus CBS sur la coupe d’arbres : les arbres de haute tiges sont valorisées à 0,01 point de CBS lorsque qu’ils sont plantés, pourquoi ne pas décompter les arbres abattus avec un malus équivalent ? On le rajoute dans le PLUm <a href="https://twitter.com/GregoryBernard?ref_src=twsrc%5Etfw">@GregoryBernard</a> ? 🤓</p>&mdash; Thomas (@tomarchi) <a href="https://twitter.com/tomarchi/status/1235868441724080132?ref_src=twsrc%5Etfw">March 6, 2020</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Il me semble que si l'on veut effectivement donner un "**nouveau souffle à la démocratie**" et éviter d'aller trop rapidement à la catastrophe, il serait bon d'être **réactif et collectif** ... 

**mais pour de vrai!!**

Parce qu'introduire le **[CBS (Coefficient Biotope par surface)](https://fr.wikipedia.org/wiki/Coefficient_de_biotope)** et être cité en exemple au niveau national (oui oui l'équipe sortante en est très fier), c'est bien!

Mais le **CBS reste un coefficient**, alors si on le fixe trop bas, ben c'est un peu comme **payer une écotaxe à 10€ sur l'achat d'un SUV**. 

Et on ne peut pas renvoyer les gens à la lecture d'un PLU de 80 pages, qui même s'il "est bien fait" n'est pas simple à utiliser sur des cas concrets pour des béotiens.

Cet exemple du PLU est très symptomatique d'**un manque de transparence plus global**.

Mettre les [conseils municipaux](https://clermont-ferrand.fr/conseil-municipal) et les [conseils métropolitains](https://www.clermontmetropole.eu/ma-metropole/linstitution-a-la-loupe/conseils-metropolitains/) sur Youtube, c'est mieux que s'il n'y avait rien, c'est sûr ... 

mais une vidéo de 4h20 qui enchaîne les votes avec des **intitulés incompréhensibles**, qui la regarde, qui la comprend? 

Même chez les conseillers municipaux, tout le monde ne comprend pas tout loin de là ...
 
Et fin du fin, dans ces conseils, il n'y a que très peu de clés de compréhension. En gros uniquement si un.e conseiller.e municipal.e a l'idée de poser une question. Tout le travail se fait en commission ...

Et devinez quoi?

Le travail de commission n'est pas du tout accessible aux citoyens. 

Pourquoi les documents énvoyés aux conseiller.e.s pour ces commissions, ne sont ils pas disponibles en ligne pour tous les administrés. 

Ce serait bien plus utilisables et instructifs qu'une vidéo de 4h00 non?

Alors, mesdames et messieurs les candidats, la transparence de la vie publique à Clermont Ferrand on commence quand?

Je vous propose de commencer ici maintenant, avec 2 documents qui ont été parfaitement absents de toute la campagne:

- [Le Plan Pluriannuel d'Investissement](/programme-pluriannuel-investissement.html)
- [Les subventions versés aux associations](/subventions-aux-associations.html)

J'ai (ou quelqu'un d'autre peut être ;)) pris la peine de demander (supplier d'obtenir) ces données, de les mettres en ligne, et de les présenter pour que **chacun puisse se faire une idée objective**, de **ce qui a été fait sous ce dernier mandat** et de **ce qui est évenbtuellemnt prévu par l'équipe sortante** pour le prochain.

Sans ces données nous parlons dans le vide, avec des candidats qui multiplient les promesses et les postures sans nous donner les moyens de juger ni de ce qui a été fait, ni de la faisabilité de ce qu'ils proposent.

**La publication des données publiques est un droit: réclamons le!**

**[Nous voulons les données pour pouvoir comprendre les décisions ici et maintenant]https://www.ted.com/talks/tim_berners_lee_the_next_web)**

PS: n'allez pas croire que j'en ai spécialement après l'équipe sortante, qui part de loin sur ce sujet. J'ai juste le sentiment que si nous ne le réclamons pas de manière appuyer, nous ne l'aurons jamais ... alors que la [Loi pour une République numérique](https://fr.wikipedia.org/wiki/Loi_pour_une_R%C3%A9publique_num%C3%A9rique) de 2016 nous y donne accès.
