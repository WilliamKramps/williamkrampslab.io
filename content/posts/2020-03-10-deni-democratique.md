Title: Déni démocratique
Date: 2020-03-10
Slug: deni-democratique

## TLDR;

Demain mecredi 11 mars à 21h, aura lieu sur [France 3 Auvergne](https://france3-regions.francetvinfo.fr/auvergne-rhone-alpes/) un grand débat des élections municipales à Clermont Ferrand.

Ont été invité: Anne Biscos, Olivier Bianchi, Jean Pierre Brenas, Eric Faidy et Marianne Maximi.

Phil Fasquel de [Cause commune](https://www.causecommune.fr/) n'est juste pas invité, alors que rien dans la campagne ne justifie de l'écarter.

Qu'un tel déni de démocratie soit perpétré par les médias, et qu'**aucun des candidats**, **à qui la démocratie est si chère**, ne s'en offusque, participe à mon sens au **délitement de la confiance des citoyens en la démocratie et en ceux qui la représentent**.

Bravo !! et bon débat entre vous!!

Si vous êtes d'accord partager sur Facebook ou twitter le boutons sont en haut de la page!

## Dans le détails

Comme vous l'aurez noté je suis très attaché au débat démocratique et au respect de toutes les [propositions qui sont faites aux clermontois](/pages/municipales2020.html).

En toute honnêteté, je n'ai pas vu quoique ce soit émaner de Marie Savre de LO (ni page Facebook, ni comptes sociaux , ni site web). [Un ou deux article dans la montagne tout au plus](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/marie-savre-et-lutte-ouvriere-veulent-faire-entendre-la-voix-des-travailleurs-lors-des-municipales-a-clermont-ferrand_13733919/#refresh).

Le RN n'a pas été très présents non plus d'après moi, mais on peut leur reconnaître une [page facebook avec des propositions claires](https://www.facebook.com/BiscosAnne) et une activité sur les réseaux sociaux. 

Je ne suis pas assez proche d'Anne Biscos pour distinguer ses [refus de participer](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/dans-le-tram-avec-pourquoi-vous-ne-verrez-pas-la-candidate-du-rassemblement-national-aux-municipales-a-clermont-ferrand-et-nous-le-deplorons_13758030/), des événements aux quelles elle n'aurait pas été invitée (ce qui est forcément arrivé, et ce qui est tout à fait regrettable à mon sens)

Pour le reste, jusque là, tout le monde avait joué le jeu:

* [A Epicentre sur la transition énergétique](https://www.epicentrefactory.com/2020/03/cinq-candidats-face-a-la-transition-ecologique/)

* [RCF avec son Débat des municipales 2020 - Clermont-Ferrand](https://rcf.fr/actualite/actualite-locale/debat-des-municipales-2020-clermont-ferrand)

* La montagne avec son "dans le tram avec ...":
    
    * [Eric Faidy](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/dans-le-tram-avec-eric-faidy-candidat-aux-elections-municipales-a-clermont-ferrand_13753565/)
    
    * [Philippe Fasquel](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/dans-le-tram-avec-philippe-fasquel-candidat-aux-elections-municipales-a-clermont-ferrand_13755266/)

    * [Marianne Maximi](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/dans-le-tram-avec-marianne-maximi-candidate-aux-elections-municipales-a-clermont-ferrand_13756924/)

    * [Olivier Bianchi](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/dans-le-tram-avec-olivier-bianchi-candidat-aux-elections-municipales-a-clermont-ferrand_13753793/)

    * [Jean Pierre Brenas](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/dans-le-tram-avec-jean-pierre-brenas-candidat-aux-elections-municipales-a-clermont-ferrand_13755289/)

    * [Anne Biscos qui n'a finalement pas eu lieu](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/dans-le-tram-avec-pourquoi-vous-ne-verrez-pas-la-candidate-du-rassemblement-national-aux-municipales-a-clermont-ferrand-et-nous-le-deplorons_13758030/)


[La liste d'Eric Faidy et Cause commune ont même débattu sur la transition écologique et la nouvelle démocratique dans un live facebook que je vous recommande](https://www.facebook.com/CauseCommuneClermont/videos/292596911720619/UzpfSTEwMDA0Mzc4NTUxNjk2MToxNDY0MTM1NzM0OTQ4MjY/).


Et bien pour [France3 Auvergne](https://france3-regions.francetvinfo.fr/auvergne-rhone-alpes/) **il y a des candidats qu'on écoute** et d'**autres qu'on passe sous silence** ...

**Je suis choqué d'un tel parti pris** et **je suis encore plus choqué de n'écouter aucune voix se lever du côté des candidats invités**.

**Il est clair qu'un tel entre soi ne pourra que donner confiance aux citoyen en la démocratie et en ceux qui prétendent la représenter!**