Title: Municipalité et Métropole
Date: 2020-02-10
Slug: municipalite-metropole

## Avez vous bien compris l'enjeu des municipales?

Comme d'habitude tout ce qui est écrit ci après, est imminemment perfectible, donc n'hésitez à me faire remonter vos corrections, commentaires, précisions via mes comptes sociaux ({% import 'includes/wk.j2' as wk %} {{ wk.social() }})

## Les élections municipales

Il faut bien comprendre que ces **15 et 22 mars** prochains nous allons faire **bien plus qu'élire un nouveau maire**.
D'une part nous allons aussi voter pour des **listes** composées de personnes qui seront amenées à faire parti du **conseil municipal** ou pas.
D'autre part  nous allons aussi voter pour des **listes** composées de(s quasi mêmes) personnes qui seront amenées à faire parti du **conseil communautaire (celui de la métropole)** ou pas.

Politiquement parlant il faut préciser également qu'il y a un enjeu national puisque les conseillers municipaux seront éventuellement amenés à voter pour un ou des délégués qui eux mêmes participeront aux [prochaines élections sénatoriales](https://fr.wikipedia.org/wiki/%C3%89lections_s%C3%A9natoriales_fran%C3%A7aises) en septembre 2020. C'est dit, ce n'est pas l'aspect que je souhaite développer ici.

## Le contexte

Pour comprendre il me semble que le mieux est de raisonner en terme de **compétences**. 

Sur un territoire il y a 3 types de collectivités

* la Région (Région AURA, président Laurent Wauquiez et son KWay rouge)
* le Département (Puy de Dôme, président Jean Yves Gouttebel ex-PS, maintenant LReM [qui a réussi l'exploit de faire démissioner 14 conseillers départementaux de sa propre majorité](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/au-conseil-departemental-du-puy-de-dome-les-14-elus-du-groupe-socialistes-et-apparentes-claquent-la-porte-de-la-majorite_13697447/))
* la Ville (Clermont Ferrand, Maire Olivier Bianchi ... il y a tant à dire ...)

<center><blockquote class="twitter-tweet"><p lang="fr" dir="ltr">🚨Christmas alerte 🚨 : j ai enfin trouvé mon cadeau pour cette année que tous ceux qui m aiment et les autres aussi se cotisent pour me l offrir. Attention ⚠️ : Choisir la plus grande taille ! <a href="https://t.co/hXRGNu30KU">pic.twitter.com/hXRGNu30KU</a></p>&mdash; Olivier BIANCHI (@olivierbianchi1) <a href="https://twitter.com/olivierbianchi1/status/1193954728247513088?ref_src=twsrc%5Etfw">November 11, 2019</a></blockquote> </center><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

En jetant un oeil sur la répartition des [compétences des collectivités locales](https://www.cohesion-territoires.gouv.fr/competences-des-collectivites-locales), on peut garder tout de même en tête que *les collectivités peuvent  déroger  à titre expérimental, et pour un objet et une durée limités, aux dispositions législatives ou réglementaires qui régissent l’exercice de leurs compétences*

Ce qui est en jeu ici c'est donc les compétences de la commune!

Mais pour Clermont-Ferrand (et d'autres communes ...) c'est plus compliqué, parce que nous sommes une métropole et que la métropole aimante de plus en plus de  [compétences issues de la commune, du département, de la région, voire de l'état](https://fr.wikipedia.org/wiki/Métropole_(intercommunalité_française)#Compétences).

Cette Métropole s'appelle [Clermont Auvergne Métrople](https://www.clermontmetropole.eu), on la désigne souvent par l'acronyme [CAM](https://www.clermontmetropole.eu) et elle fait suite à [Clermont Comunauté (ClemrontCo)](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/clermont-co-devient-une-metropole-au-1er-janvier-2018-ce-qui-va-changer_12683952/) qui elle était une [Communauté de communes (ComCom)](https://fr.wikipedia.org/wiki/Communaut%C3%A9_de_communes). 

Si je comprends ce que je lis ça veut dire que les [21 communes de la métropole](https://fr.wikipedia.org/wiki/Clermont_Auvergne_Métropole#Les_élus) font de plus en plus les choses ensembles et de la même façon.

Quand on lit la liste [des compétences de la métropoles](https://www.clermontmetropole.eu/ma-metropole/linstitution-a-la-loupe/competences/), on se demande un peu ce qu'il reste à la Ville.

J'en retiens 3 (mais vous pouvez compléter)

* la petite enfance / les écoles maternelles et élémentaires
* la police municipale (le maire est le chef)
* l'urbanisme et notamment le PLU (même si visiblement un PLU métropolitain est déjà dans les tuyaux)

Ca c'est le **contexte** qu'on devrait tous avoir en tête avant d'aller voter. 

Je vous rassure j'ai mis un petit moment à recoller les morceaux et je suis loin d'avoir saisi toutes les subtilités.


## Le scrutin

Devinez quoi? Vous ne voterez pas pour le président de la métropole! 

nan!

Celui qui décide pour l'**eau**, les **transports**, les **déchets**, l'**environnement**, l'**habitat** ... bref toutes les **compétences stratégiques et structurantes** sera élu ... par les conseillers communautaires! 

On appelle ça le 3<sup>ème</sup>tour!

J'étais ravi le jour où j'ai compris ça. D'autant que quand on est plus gros, comme à Lyon par exemple, et bien ce n'est pas le cas, le président de la métropole est élu au suffrage universel direct!

Si vous regardez la [répartition des sièges par commune](https://fr.wikipedia.org/wiki/Clermont_Auvergne_Métropole#Les_élus), vous allez vous apercevoir que la Ville de Clermont Ferrand pèse lourd!

Et donc voilà comment ca va se passer, pour toute les communes de la **CAM** (puisqu'aucune n'a moins de 1000 habitants).

**Notez que l'ordre des colisitiers dans la liste a un sens**

## Premier tour

Le premier tour est fait pour "écrémer": les **listes** faisant strictement **moins de 10%** ne seront tous simplement **pas présentes au second tour**. 
En toute logique les personnes présentes sur ces listes sortent de l'élection et les autres restent ...

**MAIS!**

## Second tour

[Au second tour on peut remodeler les **listes**](http://www.doubs.gouv.fr/Politiques-publiques/Citoyennete-Elections/Les-Elections/Elections-politiques/Elections-municipales/Etre-candidat-au-2eme-tour-des-elections-municipales-2020)!

Les **listes qui se présentent au second tour** peuvent faire des **alliances** avec

- les **listes** ayant fait **plus de 10% qui** ne se présentent pas au second tour
- les **listes** ayant fait **moins de 10%** mais plus de **5%**

**Faire alliance** c'est essentiellement **virer des noms d'une liste** pour **caser ceux d'une autre liste** et j'imagine négocier l'ordre.

**Mais ce n'est pas si simple**: En [2014](https://fr.wikipedia.org/wiki/%C3%89lections_municipales_de_2014_%C3%A0_Clermont-Ferrand) par exemple Alian Laffont pouvait prétendre au second tour, et il s'est allié avec Bianchi. **MAIS** il a bien précisé à Bianchi que ceux de sa liste seraient dans l'opposition au sein du conseil municipal.

## La répartition des sièges

*Les sièges du conseil municipal sont répartis comme au premier tour, avec l'application d'une prime majoritaire avec répartition proportionnelle à la plus forte moyenne.*

C'est là où l'**ordre du colisitier** dans la liste joue, la **tête de liste** passe en premier (on comprend pourquoi on l'appelle comme ça du coup), et **plus le colistier est haut dans la liste plus il a de chances de siégier** au conseil municipal.

A **Clermont-Ferrand**, il y avait **55 membres** pour ce dernier mandat. Si je comprends bien [18 pour la majorité](https://clermont-ferrand.fr/1-le-maire-et-ses-adjoints-ville-de-clermont-ferrand) et [20 pour l'opposition](https://clermont-ferrand.fr/les-conseillers-municipaux) (si si refaites le calcul!).

On se dit au passage que ça fait un paquet de sensibilités politiques différentes autour de la table, ce qui ne doit pas toujours forcément aider pour la prise de décision.

## Et la Métropole?

Ah oui! La métropole.

En fait ça marche pareil avec les mêmes listes dans le même ordre, [à quelques cas particuliers prêts qui ne me paraissent pas changer ma compréhension de l'éléction](https://www.vie-publique.fr/eclairage/271171-municipales-2020-conseillers-communautaires-les-regles-de-lelection#titre_1). 

Il y aura [84 membres dont 38 pour Clermont Ferrand](https://www.clermontmetropole.eu/fileadmin/user_upload/Conseils_communautaires/2019_05_17_-_Conseil_Metropolitain_du_17_mai_2019/Administration_Generale/DEL20190517_002.pdf)

Et ils élisent le président de la métropole (dans des conditions qui restent à préciser en ce qui me concerne, mais il faut bien s'arrêter un jour).

Reste à savoir comment la prise de décision se fait effectivement, et comment le jeu de la proportionnalité des pouvoirs s'exercent.

Un truc qui me semble bien pour se faire une idée est de regarder des extraits de [conseils municipaux](https://www.youtube.com/watch?list=PLnwA-WijLGdHQJAjxezujFqPmREko8kfR&v=L4UBuy49Dds) ou de [conseil communautaire](https://www.youtube.com/watch?v=LvT8vyrbfAw&list=PLqpLSjSTIobTF410TrcJlReO7Lx_4suMJ) ...

Bon courage!

On parle de prises de décisions sur des sujets tous très différents, avec confrontations de points de vue très hétérogènes pendant 3 à 7 heures!

## Conclusion

**Le temps est un vrai problème**: il en reste peu, on en manque souvent et comprendre comment fonctionne une métropole comme la nôtre ça ne peut pas se faire en un claquement de doigts.

Pourtant si on veut [**arriver à s'organiser un peu**](/pages/resilience.html) en cas de **[pépin](/pages/effondrement.html)** je crois que ce serait bien qu'on soit plein à s'y intéresser.

Pas forcément tous, ni à tout. Mais au moins sur les sujets d'experts et ceux qui nous tiennent à coeur.

Pour ça il faut que les élus nous mettent à bord ... encore plus je veux dire! qu'ils soient pédagogiques et transparent sur les faits.

Il faut également qu'on arrive **nous citoyens** à passer du temps, **individuellement**, **collectivement** pour prétendre pouvoir participer à tout moment.

Je garde en tête que *les collectivités peuvent  déroger  à titre expérimental, et pour un objet et une durée limités, aux dispositions législatives ou réglementaires qui régissent l’exercice de leurs compétences* ... 

je ne suis pas sûr de comprendre ce que ça permet de faire, mais j'aime bien l'idée, pas vous?