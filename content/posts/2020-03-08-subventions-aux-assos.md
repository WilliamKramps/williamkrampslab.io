Title: Subventions faites aux associations
Date: 2020-03-08
Slug: subventions-aux-associations

il s'agit ici de présenter l'évolution des subventions faites aux associations sur 6 ans (de 2014 à 2019).

Je vous propose un visualisation dite "multiline chart" qui permet de bien se rendre compte des évolutions. 
Comme j'ai bricolé ça vite fait, je ne vous garantis pas l'accessibilité sur smartphone, mieux vaut regarder ça sur une ordinateur avec une bonne résolution d'écran.

[Subventions faites aux associations par grandes catégories d'associations](/html/assos.html)

* [ACCOMPAGNEMENT DES ENTREPRISES](/html/assos.html#accompagnement-des-entreprises)

* [DECHETS](/html/assos.html#dechets)

* [DEVELOPPEMENT CULTUREL](/html/assos.html#developpement-culturel)

* [DEVELOPPEMENT DURABLE](/html/assos.html#developpement-durable)

* [DIVERS](/html/assos.html#divers)

* [ENSEIGNEMENT SUP RECHERCHE INNOVATION](/html/assos.html#enseignement-sup-recherche-innovation)

* [HABITAT POLITIQUE DE LA VILLE](/html/assos.html#habitat-–-politique-de-la-ville))

* [SPORTS](/html/assos.html#sports)

* [TOURISME](/html/assos.html#tourisme)