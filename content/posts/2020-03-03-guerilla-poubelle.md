Title: Guerilla poubelle
Date: 2020-03-03
Slug: guerilla-poubelle

Ca ne vous aura pas échappé, ça souffle souvent fort dans les rues de Clermont!

Et le premier truc qui vole dans ces cas là ... c'est nos ordures!

![zef](images/zef.jpg)

Une bonne occasion de constater que

1. [le meilleur déchet est celui qu'on ne produit pas!](https://www.facebook.com/ZDClermontAuvergne/)
2. [quand on voit le contenu d'une poubelle jaune on se rend compte que tout le monde n'a pas compris ce qu'on devait mettre dedans](https://www.clermontmetropole.eu/preserver-recycler/gestion-des-dechets/trier-ses-dechets)
3. les éboueurs font un métier indispensable à tous, [pas forcément des plus agréables](https://www.youtube.com/watch?v=5jfyDJVhMUo), [payé au SMIC, avec une espérance de vie bien en de ça la moyenne](https://fr.wikipedia.org/wiki/%C3%89boueur).
4. oui! certaines poubelles ont un système de verrouillage, mais c'est loin d'être généralisé

Lambeaux de plastiques accrochés aux arbres, pots de yaourt et autres briques de lait sur les trottoirs, dans les parcs: est on capable collectivement d'agir là dessus?

On peut attendre que les pouvoirs publics changent tous les bacs pour des bacs verrouillés ou on peut agir ici et maintenant avec ... 

**un simple tendeur élastique pour vélo!**

![Guerilla Poubelle](images/guerilla-poubelle-1.jpg "Guerilla Poubelle")
![Guerilla Poubelle](images/guerilla-poubelle-2.jpg "Guerilla Poubelle")
![Guerilla Poubelle](images/guerilla-poubelle-3.jpg "Guerilla Poubelle")
![Guerilla Poubelle](images/guerilla-poubelle-4.jpg "Guerilla Poubelle")

* si la poubelle tombe le couvercle ne s'ouvre pas. 
* déverrouiller le couvercle est facile et ne pénalise pas les éboueurs sur leurs tournées (merci pour eux!)
* si le dispositif est bien attaché il reste en place pour la prochaine utilisation sans avoir à reverrouiler
* c'est pas cher, on peut en acheter plusieurs et les distribuer à ses voisins, on peut même en poser de manière sauvage, sur des poubelles renversées qu'on croise: vu la simplicité du dispositif on peut supposer que le propriétaire de la poubelle comprendra l'intérêt et l'utilisera par la suite.

Et, cerise sur le gâteau, **si on est plein à le faire, plein d'autres le feront naturellement**, parce que c'est malin, pas cher et facile à reproduire ... 

Imaginez la fierté collective qu'on éprouverait à constater qu'on est capable de régler un problème comme celui ci: sans Veolia, sans la métropole ... Juste nous!

**Tous ensembles, clermontois.e.s modestes et géniaux, lançons la guerilla poubelle!**


