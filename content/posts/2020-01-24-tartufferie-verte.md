Title: Interpellation des candidats Clermontois au sujet de la tartufferie verte de la transition écologique
Date: 2020-01-24
Slug: tartufferie-verte

<center><blockquote class="twitter-tweet"><p lang="fr" dir="ltr">La lutte contre le réchauffement climatique est le défi contemporain le plus important pour nos sociétés. <br><br>Avec des élus nous appelons à la mise en place d&#39;une COP26 des territoires, pour apporter des solutions aux problématiques et fédérer nos énergies.<a href="https://t.co/6FhMV90Q5s">https://t.co/6FhMV90Q5s</a></p>&mdash; Olivier BIANCHI ([Olivier Bianchi](https://twitter.com/olivierbianchi1)) <a href="https://twitter.com/olivierbianchi1/status/1217006414452920320?ref_src=twsrc%5Etfw">January 14, 2020</a></blockquote></center><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Voici ce que [Olivier Bianchi](https://twitter.com/olivierbianchi1) a d'épingler actuellement sur son compte twitter.

On épinlge un tweet pour que tout le monde le voit, on épingle parce que c'est important!

[Eric Faidy](https://twitter.com/EricFaidy), [Jean Pierre Brenas](https://twitter.com/jpbrenas) auraient pu épingler un article envoyant exactement le même signal d'engagement main sur le coeur. **Cette interpellation vaut pour leurs programmes également qui me semblent aussi "tartuffes" que celui de Bianchi**.

J'invite **les autres candidat.e.s** (par ordre alphabétique [Anne Biscos](https://twitter.com/annebiscos), [Clermont en Commun](https://twitter.com/ClemrontCommun), [Cause Commune](https://twitter.com/cause_commune), [Michel Fanget](https://twitter.com/michelfanget)), **à se sentir également concerné.e.s par cette interpellation**. Je les distingue car je n'ai pas pu lire à date leurs propositions sous une forme définitive (dépêchez vous!)

## je reproduis ici [mon interpellation sur ce tweet épinglé pour un meilleure lisibilité](https://twitter.com/olivierbianchi1/status/1217006414452920320)

---

L'article dit "Au-delà des postures, des thèmes vagues ou du concours Lépine de mesures d’affichage, il est urgent de transformer collectivement notre modèle de développement local".

C'est de vos postures dont on parle ici [Olivier Bianchi](https://twitter.com/olivierbianchi1), [Eric Faidy](https://twitter.com/ericFaidy) et [Jean-Pierre Brenas](https://twitter.com/jpbrenas)!

---

[La situation et les leviers à actionner sont connus depuis longtemps](https://www.orcae-auvergne-rhone-alpes.fr/fileadmin/user_upload/mediatheque/orcae/Profils_v1/Profil_246300701.pdf) ...

65% des GES sont produits par la gestion des déchets cumulée, aux transports routier.

---

Je me permets ici de reprendre les points de "tartufferie verte" de votre [programme](https://olivierbianchi2020.fr/wp-content/uploads/2020/01/Programme2020_vDEF.pdf) (une fois de plus je peux faire le même exercice chez [Eric Faidy](https://twitter.com/ericFaidy) et [Jean-Pierre Brenas](https://twitter.com/jpbrenas).

---

un "budget carbone" qui évalue quoi? quels leviers allez vous actionner? Pour quels résultats intermédiaires? à quel échéance? 

Un territoire à énegrie positive en 2050 est bien trop ambitieux pour rester aussi vague sur le "comment?"
un maire responsable, détaillera, priorisera et expliquera courageusement le pourquoi de ses mesures.

---

une "ceinture maraîchère": non! on doit penser [**REGIE AGRICOLE COMMUNE**](https://www.monmandatlocal.fr/collectivites/produire-local-bio-pari-ambitieux-regies-agricoles-communales/) et [**résilience du territoire**](https://williamkramps.gitlab.io/pages/resilience.html)

---

il faut également consigner le risque majeur d’une **RUPTURE DE LA CHAINE DE DISTRIBUTION ALIMENTAIRE** dans le [**DICRIM**](https://fr.calameo.com/books/000000209b0e8c2ea0a41)

---

"végétaliser la ville" ... en ne renoncant à aucun projet de construction / extension menés au pas de course? est ce bien sérieux?

---

un "permis de végétaliser"? Quelle blague? on devrait rémunérer ceux qui végétalisent en service publique! oui! on ne délivre par de permis d'améliorer la situation

---

Programme d’"incitation à la végétalisation" des toits et façades: à destination de qui? de moi ou des hypermarchés? On ne réprésente pas le même impact en terme de surface pour le séquestre des GES non?

---

"En développant l’énergie photovoltaïque par l'investissement public et privé (notamment citoyen)" il faut parler de **REGIE ENERGETIQUE**!

---

"Construction d’un nouveau réseau de chaleur à Saint-Jacques qui utilisera l'énergie résiduelle de l'incinérateur de déchets." Oui!

 mais on arrivera pas à gérer et à minimiser nos déchets autrement qu'en les traitants nous mêmes, par une **REGIE DES DECHETS** encore ... déconnectée d'une logique de profit!

---

Il faut changer la facon de financer tout ça! l'investissement, et les aides de l'état ok! mais il faut aussi qu'on relocalise notre économie dans un système cohérent de valeurs locales, qui favorise à tout prix les biens qui n'ont parcouru que peu de kilomètres ... 

Nous avons déjà des initiatives comme la [**doume**](https://doume.org/) ou  [**duniter**](https://duniter.org/fr/comprendre/) qui ne sont pas parfaites mais qui ne demandent pour le coup qu'à se développer ...

---

Voilà ce qui me semble faire parti un plan d'attaque pour "Agir face à l’urgence écologique" ... et votre programme à vous [Olivier Bianchi](https://twitter.com/olivierbianchi1) (mais aussi ceux de [Jean-Pierre Brenas](https://twitter.com/jpbrenas) et [Eric Faidy](https://twitter.com/ericFaidy)) me semble hélas assez éloigné de cela.

---

Comprenons nous, il ne s'agit pas d'arrêter l'investissement, de bouder les aides, de faire fuire les entreprises, de si tirer une balle dans le pied!

L'état, le privé et les administrés ont en commun la préoccupation des sécurités alimentaire, hydrique, énegrétique, économique et sociale des français, des employés, et infine des clermontois.

--- 

Il s'agit de développer un **système parallèle**, **local**, **vertueux**, le reste continuera d'exister le temps qu'il lui reste à vivre dans tous les cas. Le reste n'a pas besoin du soutien de la mairie ou de celui de la métropole. 

En revanche **nous bénéficierons tous d'un système local parallèle** pré existant en cas de durcissement causé par des **crises climatiques**, **économiques**, **énergétiques**, **sanitaires**, y compris **les entreprises**!

---

Et ne me renvoyez pas mon idéalisme à la figure: certains maires ont déjà [entamé la transtion](https://sosmaires.org/communes_en_pointe/) ... oui ce sont de petites communes ... Et oui être de maire de Clermont pour le mandat 2020-2026 c'est relevé un vrai défi!

EDIT: [Causes Communes](https://twitter.com/cause_commune) m'a signalé qu'ils avaient signé la charte des maires en résilience le 10 janvier

---

Mon propos est intelligible par tous, et ne demande que de la volonté politique: celle de vouloir "Agir face à l’urgence écologique". 

---

Ne m'opposer pas **les compétences de la Métropole face à celle de la municipalité**. Oui c'est bien au niveau de la métropole qu'il faut agir et cela fait bien parti du mandat pour lequel vous sollicitez votre réelection auprès des clermontois.

---

En espérant voire votre programme (ou vos programmes [Jean-Pierre Brenas](https://twitter.com/jpbrenas) et [Eric Faidy](https://twitter.com/ericFaiy) encore évoluer pour se hisser à la hauteur des enjeux, que nous savons bien au delà des "engagements" qui n'aboutissent qu'au consensus mou et à l'inaction.

---

Clermont capitale européenne de la culture, Clermont la gagnante, Clermont capitale de la mobilité ... si le projet "**Clermont la résiliente**" n'est pas votre priorité nous perdrons tout à la première secousse!

---

Bien à vous 3 [Olivier Bianchi](https://twitter.com/olivierbianchi1) [Jean-Pierre Brenas](https://twitter.com/jpbrenas) et [Eric Faidy](https://twitter.com/ericFaiy). et bien à vous autres candidat.e.s [Anne Biscos](https://twitter.com/annebiscos), [Causes Communes](https://twitter.com/cause_commune), [Clermont en Commun](https://twitter.com/ClermontCommun), [Michel Fanget](https://twitter.com/michelfanget) que je distingue car je n'ai pas encore pu consulter vos propositions finalisées à date!

--- 
[Cette interpellation dans les commentaires twitter d'Olivier Bianchi](https://twitter.com/olivierbianchi1/status/1217006414452920320)
