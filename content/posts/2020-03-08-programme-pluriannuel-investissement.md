Title: Programme pluriannuel d'investissement à Clermont Ferrand
Date: 2020-03-08
Slug: programme-pluriannuel-investissement

Si j'ai bien compris (mais je peux me tromper et je rectififerai si on m'apporte des précisions), il s'agit ici de comparer les dépenses engagées en 2017, avec les dépenses prévues pour 2018-2025. 
Il faut comprendre que dans la **tranche 2018-2025 certaines dépense ont déjà été engagées et d'autres non**.

Je vous propose un visualisation dite "slope chart" qui permet de bien se rendre compte des évolutions. 
Comme j'ai bricolé ça vite fait, je ne vous garantis pas l'accessibilité sur smartphone, mieux vaut regarder ça sur une ordinateur avec une bonne résolution d'écran.

[Programme pluriannuel d'investissement par grandes catégories](/html/ppi.html)

* [Métropole créative, attractive et rayonnante](/html/ppi.html#metropole-creative--attractive-et-rayonnante) (il faut être TES patient mais ça finit par s'afficher ... normalement)

* [Métropole pour entreprendre](/html/ppi.html#metropole-pour-entreprendre)

* [Développement territorial équilibré et solidaire](/html/ppi.html#developpement-territorial-equilibre-et-solidaire)
(il faut être TES patient mais ça finit par s'afficher)

* [Métropole durable](/html/ppi.html#metropole-durable)

* [Projets communautaires](/html/ppi.html#projets-communautaires)