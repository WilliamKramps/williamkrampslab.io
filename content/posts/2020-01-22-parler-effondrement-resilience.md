Title: parler effondrement et résilience
Date: 2020-01-22
Slug: parler-effondrement-resilience

Parler [effondrement](/pages/effondrement.html) et [résilience](/pages/resilience.html), entre personnes ayant déjà vécu le choc de la prise de conscience peut s'avérer très enrichissant, voire essentiel: on échange des lectures, des références, des points de vue et on peut commencer ici et maintenant à anticiper ensemble.

Parler [effondrement](/pages/effondrement.html) et [résilience](/pages/resilience.html) avec le reste du monde, c'est à dire avec des personnes n'ayant pas encore réalisé l'ampleur et l'imminence du sujet, peut se révéler assez frustrant. 

Personnellement j'ai croisé différents types de profile (n'hésitez pas à m'en suggérer d'autres)

- les **climatosceptiques** ... ça commence heureusement à se faire rare. Avec eux il n'y a pas grand chose à faire ... leur faire entendre que [2019 est au 3e rang des années les plus chaudes en France](http://www.meteofrance.fr/actualites/77869783-climat-2019-au-3e-rang-des-annees-les-plus-chaudes-en-france), serait déjà un pas en avant.

- les **autruches** qui partagent l'intuition de l'[effondrement](/pages/resilience.html) mais qui refusent de le prendre en compte par peur. Je pense qu'il ne faut pas trop insister ... c'est un choc suffisament brutal pour ne pas l'imposer frontalement à quelqu'un. 

- les **[solutionnistes](https://la-rem.eu/2015/04/solutionnisme/)** qui pensent que l'humain s'en est toujours tiré jusque là, face à des choses qui auraient pu le détruire (épidémie, famine, guerre, etc ...). Le raisonnement sous tend qu'une solution va être touvée en temps en heure ... Cela revient, en 2020, à s'en remettre à la technologie. Or à date il n'y a rien qui résolve ni le réchauffement climatique, ni l'épuisement des ressources, ni les crises financières, ni les conflits internationaux ... 
et ceux qui vous diront que c'est l'intelligence artificielle qui va résoudre ces problèmes n'ont juste pas compris qu'en 2020 et pour encore des années [l'intelligence artificielle n'existe pas](https://www.lepoint.fr/video/luc-julia-l-intelligence-artificielle-n-existe-pas-02-02-2019-2290914_738.php). De manière plus général il est tout à fait hasardeux de miser sur le fait que la technologie va résoudre les problèmes qu'elle a engendrés.

- les **Classes sup** qui pensent que quoiqu'il arrive ils s'en sortiront mieux que les autres. Souvent parce qu'ils considèrent qu'ils ont plus d'argent. Ce raisonnement est un peu court face à un [effondrement](/pages/resilience.html) systémique: s'il fontcionne pour un milliardaire de la [Silicon Valley](https://www.franceculture.fr/emissions/la-vie-numerique/pourquoi-les-millionnaires-de-la-silicon-valley-se-preparent-la-fin-du), il risque de s'avérer fragile pour un cadre clermontois, même brillant et très bien payé. La question peut se reformuler ainsi: "Avez vous de quoi vous payez une milice privée? Pendant combien de temps avant qu'elle ne se retourne contre vous?".

- les "**accélérationnistes**" comme tout est parti à merder, faisons que tout merde encore plus vite! on trouve pas mal d'**accélérationnistes** chez les [*libertariens*](https://fr.wikipedia.org/wiki/Libertarianisme) (ou l'inverse).

- les **fatalistes** qui disent que l'homme ne sera pas la première espèce animal à s'éteindre. Transposer ce raisonnement est assez amusant: "Si je te mets sur un bûcher, sauras tu accepter ton sort sous prétexte que tu ne seras pas le premier à mourir calciné?"

- les **survivalistes** sont une catégorie à part. Ils ne sont pas forcément sceptiques sur le sujet de l'[effondrement](/pages/resilience.html), mais plutôt sur celui de la [résilience](/pages/resilience.html). Ils pensent qu'en étant armés et en faisant des provisions, ils pourront résister plus longtemps que les autres. La question qui se pose est celle de l'après ... Etre sûr d'avoir toutes les compétences pour pouvoir survivre en autonomie est un pari risqué, préparer une communauté résiliente me paraît multiplier les chances de réussite.

Tous aboutissent in fine à la même chose: le **déni** et / ou l'**immobilisme** qui mènent au même résultat, continuer commme d'habitude ... 

les collapsologues appellent ça le mode "*business as usual*".

Pour échanger avec ce genre de "**sceptiques**" je vous invite à lire les [10 conseils pour être l'inspirateur, et non l'emmerdeur de service](https://www.collaborativepeople.fr/single-post/2019/12/19/10-conseils-pour-%C3%AAtre-linspirateur-et-non-lemmerdeur-de-service-%C3%A0-No%C3%ABl) afin d'échanger efficacement et en douceur.

Face aux scepticisme, n'oubliez jamais que mécaniquement, à chaque instant, **de nouvelles personnes prennent conscience de l'inéluctabilité et de l'imminence d'un [effondrement](/pages/resilience.html)**. 

Ca ne marche que dans un sens: **il y a peu de chance que quelqu'un qui a pris consicence de la probabilité d'un [effondrement](/pages/resilience.html) fasse le chemin inverse**. 

Ceci nous rend **de plus en plus nombreux** et donc **de plus en plus efficaces** pour absorber les chocs et pour sensibiliser de nouvelles personnes.

Notez, que les municipales sont une opportunité pour entamer un projet de [résilience](/pages/resilience.html) à l'échelle d'une ville, et qu'à mon sens sensibiliser nos candidats à cette problématique est une urgence.

C'est le sens de ma démarche!

A voir:

* [Imaginaires des futures possibles. Effondrement : seul scénario réaliste ? Par Arthur Keller](https://www.youtube.com/watch?v=kLzNPEjHHb8)
* [Comment parler de la fin du pétrole, par Anthony Bault](https://www.youtube.com/watch?v=ImGLPH3eIdE)
* [Une Résilience intérieure #ECOPSYCHOLOGIE par Pablo Servigne](https://www.youtube.com/watch?v=iJ_NBs_huBs)
* [Didier Super & le déni écologique](https://www.youtube.com/watch?v=FdZFzEaCLkU)
