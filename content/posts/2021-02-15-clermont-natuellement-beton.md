Title: Clermont Naturellement Béton
Date: 2021-02-14
Slug: clermont-naturellement-beton

## Résumé (TLDR;)

"Clermont Naturellement Béton" est une intitiative citoyenne visant à cartograhier, de manière collaborative, les transformations urbanistiques de [Clermont Auvergne Métropole](https://en.wikipedia.org/wiki/Clermont_Auvergne_M%C3%A9tropole) et notamment leur impact sur l'environnement.

Pour participer, publiez vos photos en précisant l'adresse, la ville (la cartographie est métropolitaine), et si vous le pouvez, le promoteur, la surface sur l'un des 3 médias suivant:

* [<i class="fab fa-facebook"></i> le groupe facebook](https://www.facebook.com/groups/clermontnaturellementbeton)
* [<i class="fab fa-twitter"></i> le hastag twitter #ClermontNaturellementBeton](https://twitter.com/hashtag/ClermontNaturellementBeton)
* [<i class="fas fa-map-marker-alt"></i> la cartographie sur framamap](https://framacarte.org/fr/map/anonymous-edit/93899:bvYrlmLxH1RLSUIonUaknSWE6q8)

Une équipe d'administrateurs, s'occupera ensuite de structurer et de mutualiser vos informations afin de les présenter dans un [<i class="fas fa-file-csv"></i> tableur de données brutes (CSV)](https://williamkramps.gitlab.io/data/clermont-naturellement-beton.csv)

Cette démarche s'inscrit dans la logique des [données ouvertes (encore appelées open data)](https://fr.wikipedia.org/wiki/Donn%C3%A9es_ouvertes) qui est déjà en place dans de nombreuses métropoles:

* [Paris](https://opendata.paris.fr/pages/home/)
* [Rennes](https://data.rennesmetropole.fr/explore)
* [Lille](https://opendata.lillemetropole.fr/pages/home/)
* [Grenoble](https://data.metropolegrenoble.fr/)

... mais pas à [Clermont Auvergne Métropole](https://en.wikipedia.org/wiki/Clermont_Auvergne_M%C3%A9tropole) :/

## Bientôt un an que nous sommes "Naturellement Clermont"

Ce fut une année bien difficile. 

Pour tous.

Nous avons beaucoup appris: ce qui est essentiel, ce qui ne l'est pas, ce qui devrait l'être ...

Les plus gros boulversements sont, hélas, probablement à venir, tant sur le plan économique, qu'écologique.

Nous sommes nombreux à penser que les deux se répondent et qu'il est grand temps de se préocupper de notre territoire afin que l'on puisse encore l'envisager comme habitable et vivable à horizon 10 ans.

Nous sommes nombreux également à nous interroger, sur les questions d'urbanisme à Clermont Ferrand, et si l'équipe sortante à tenter de nous persuader de son virage écologique à grand renfort d'[alliance EELV](https://olivierbianchi2020.fr/notre-equipe/) et de [vert sur ses tracts](https://olivierbianchi2020.fr/une-ville-nature/), force est de constater que le virage n'a été perçu par aucun d'entre nous.

Au contraire nous voyons fleurir les panneaux de promoteur immobilier, sur des espaces herborés et jaillir d'immenses constructions là où l'on nous annoncer des [éco quartiers](http://www.ecoquartiers.logement.gouv.fr/le-label/).

## Inititative citoyenne

Beaucoup de questions restent sans réponse:

- le [Plan Local d'Urbanisme](https://clermont-ferrand.fr/plu) et le fameux [Coefficient Biotope par Surface](https://www.o2d-environnement.com/observatoires/coefficient-de-biotope-par-surface/) sont ils à la hauteur des enjeux?
- Quel est le besoin en nouveaux logements?
- Combien de logements sont actuellement vides?
- Combien de projets sont en cours de constructions?
- Les acteurs locaux de la construction sont ils les gagnants des transformations urbaines?
- Quel est l'impact de cette explosion de défiscalisation immobilière sur la valeur du parc immobilier clermontois en général?
- Pourquoi condamner les dernières friches et espace de végétation?

et bien d'autres encore que je laisse à chacun le soin de forumler ...

Ces questions ont été posées par les citoyens, par les oppositions, en conseil métropolitain ... et sont restées sans réponse.

La Métropole joue-t-elle l'opacité?

Est elle seulement en mesure de fournir ces données basiques?

Devant ce silence assourdissant  de la métropole et l'inquiétude grandissante des clermontois, **Clermont Naturellement Béton** propose une **initiative citoyenne** permettant à tous de recenser des données objectives, afin que chacun puisse se faire une idée sur le dénombrement, et les impacts de ces transformations en cours dans notre ville.

Plus précisément pour que chacun puisse se rendre compte de ce qui se construit, de ce qui va se construire et de ce qu'il reste à construire ... ou à préserver ...

Cette initiative a pour ambition:

1. de cartographier au niveau de [Clermont Auvergne Métropole](https://en.wikipedia.org/wiki/Clermont_Auvergne_M%C3%A9tropole):

   - les espaces vierges: friches, jardins, etc
   - les projets à venir: panneau de promoteur, panneau de chantier, etc
   - les projets en cours: panneau de promoteur, panneau de chantier, etc
   - les projets récemments terminés
   - peut être d'autres types de terrain auxquels nous n'avons pas encore pensé

2. de comprendre les concepts liés à l'urbanisme en général: PLU, CBS, eco-quartier, Patrimoine, ...
3. d'appréhender différentes manières d'évaluer l'impact des trasformations urbanistiques sur l'environnement.

## On a besoin de vous!

Cette initiative est ouverte à tous et doit être utilisable par tous.

Elle se veut dépassionnée et objective: même s'il y a beaucoup d'amertume dans nos constats, nous nous emploierons à nous en tenir au fait. 

Vous avez identifié des friches, des jardins, des panneaux de construction, des constructions imposantes, en construction ou récemment terminées ?

Les constructions vous paraissent incompatibles avec ce qui a été déclaré pendant la campagne des Muncipales à propos de l'urbanisme:

- "La ville a cessé de s'étendre"
- "La ville se reconstruit sur elle même"
- "Toute surface vierge occupée par une construction est rendue à la nature"

### Partagez vos photos

* Sur le [<i class="fab fa-facebook"></i> le groupe facebook](https://www.facebook.com/groups/clermontnaturellementbeton)
* Avec [<i class="fab fa-twitter"></i></a> le hastag twitter #ClermontNaturellementBeton](https://twitter.com/hashtag/ClermontNaturellementBeton)
* Directement sur [<i class="fas fa-map-marker-alt"></i>la cartographie sur framamap](https://framacarte.org/fr/map/anonymous-edit/93899:bvYrlmLxH1RLSUIonUaknSWE6q8)
* En DM [twitter](https://twitter.com/William_Kramps), [messenger Facebook](https://www.facebook.com/william.kramps.63) ou par mail [william.kramps@protonmail.com](mailto:william.kramps@protonmail.com) si vous ne voulez pas poster avec votre compte perso ... 
  * je me ferai un plaisir de respecter votre [anonymat auquel je suis tant attacché](https://williamkramps.gitlab.io/pseudonymat.html)!

Ensuite une équipe d'administrateur s'occupera de les cartographier.

La progression de notre cartographie est acccessible à tous

- Sur [<i class="fas fa-map-marker-alt"></i> la cartographie framamap](https://framacarte.org/fr/map/anonymous-edit/93899:bvYrlmLxH1RLSUIonUaknSWE6q8)
- Dans un [<i class="fas fa-file-csv"></i> tableur de données brutes (CSV)](https://williamkramps.gitlab.io/data/clermont-naturellement-beton.csv)

### Partagez votre expertise

Vous avez une expertise en urbanisme, en immobilier, ou sur les problématiques environnemetales liées à l'urbanisme?

Vos commentaires, vos publications (pour des explications longues [ce blog reste ouvert aux contributions](/pages/appel-a-contributions.html)) seront précieux pour tous les participants et permettront de consolider le bilan que nous prétendons dresser, autant que de dépasser les amertumes.

### Faites grossir l'intiative

N'hésitez pas à partager [ce post](https://williamkramps.gitlab.io/clermont-naturellement-beton.html), l'[<i class="fas fa-map-marker-alt"></i> adresse la cartographie sur framamap](https://framacarte.org/fr/map/anonymous-edit/93899:bvYrlmLxH1RLSUIonUaknSWE6q8), l'[<i class="fab fa-facebook"></i> adresse du groupe facebook](https://www.facebook.com/groups/clermontnaturellementbeton), en publiant sur [<i class="fab fa-twitter"></i> le hastag twitter #ClermontNaturellementBeton](https://twitter.com/hashtag/ClermontNaturellementBeton) ...

A titre personnel, par exemple, quand je lis des commentaires désabusés sur tel ou tel projet, je commente en copiant l'url du groupe et en ajoutant un "tu viens?" ;)

## Qui est derrirère cette intitiative citoyenne

Vous!

Si je suis à l'initiative de cette initiative citoyenne, elle ne m'appartient pas!

* Vous pouvez devenir administrateur du groupe facebook
* Vous pouvez aussi mettre vos compétences techniques au  service de cette initiative afin d'améliorer la collecte et la cartographie
* Vous pouvez également proposer un groupe de travail sur une thématique précise et l'animer


N'hésitez pas à me contacter par DM [twitter](https://twitter.com/William_Kramps), [messenger Facebook](https://www.facebook.com/william.kramps.63) ou par mail [william.kramps@protonmail.com](mailto:william.kramps@protonmail.com)

Si vous restez sceptique, où que vous vous interrogez encore sur les motivations profondes de cette initiative dites vous que TOUT ce qui sera collecté sera accessible à TOUS!

En espérant vous comptez parmis nous!
