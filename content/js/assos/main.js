function dataviz(data) {

  var canvas = document.getElementById("barChart");
  var ctx = canvas.getContext('2d');

  Chart.defaults.global.defaultFontColor = 'black';
  Chart.defaults.global.defaultFontSize = 16;
  
  // Notice the scaleLabel at the same level as Ticks
  var options = {
    
    scales: {
      yAxes: [{
          ticks: {
              beginAtZero:true
          },
          scaleLabel: {
              display: true,
              labelString: 'Montant versé en €',
              fontSize: 20 
            }
      }]            
    }  
  };

  if(hashExists ()) {
    options['title'] = {
      display: true,
      text: decodeURI(location.hash.substr(1))
    }
  }
  else {
    options['title'] = {
      display: true,
      text: 'Subventions associations à Clermont Ferrand'
    }
  }

  // Chart declaration:
  var myBarChart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: options
  });
}

$.getJSON(selectUrl (),function(result){

  colors = ["yellow","red","green", "gray", "brown", "cyan", "purple", "blue", "magenta", "orange", "pink", "olive","teal","fuchsia","navy","lime","silver"];
  var data = { 
    labels: ["2014", "2015", "2016", "2017", "2018", "2019"],
    datasets: []
  };

  result.forEach(function(item, index, array) {
    data["datasets"].push({
      label: item.name,
      fill: false,
      lineTension: 0.1,
      borderColor: colors[index], // The main line color
      borderCapStyle: 'square',
      borderDash: [], // try [5, 15] for instance
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "black",
      pointBackgroundColor: "white",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      data: item.values,
      spanGaps: true,
    });
  });
    
  console.log(data);
  dataviz(data);

});