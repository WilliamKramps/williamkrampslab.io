function dataviz() {
    // config svg
    var margin = {top: 100, right: 400, bottom: 180, left: 120};
    
    var width = 800 - margin.left - margin.right;
    var height = 600 - margin.top - margin.bottom;
    var formatAmount = d3.format(",");

    console.log(width);
    console.log(height);
        
    var svg = d3.select("#viz").append("svg")
            .attr("id", "slope-svg")
            .attr("width", 1280)
            .attr("height", height + margin.top + margin.bottom)
        .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var y1 = d3.scaleLinear()
        .range([height, 0]);

    var config = {
        xOffset: 0,
        yOffset: 0,
        width: width,
        height: height,
        labelPositioning: {
            alpha: 0.5,
            spacing: 18
        },
        leftTitle: "2017",
        rightTitle: "2018-2025",
        labelGroupOffset: 5,
        labelKeyOffset: 120,
        radius: 6,
        // Reduce this to turn on detail-on-hover version
        unfocusOpacity: 0.3
    }

    div = d3.select("body")
        .append("div") 
        .attr("class", "tooltip");

    d3.json(selectUrl(), function(error, data) {
        if (error) return error;
        
        // Combine datas into a single array
        var datas = [];
        
        data._2017.forEach(function(d) {
            d.year = "2017";
            datas.push(d);
        });
        
        data._2018_2025.forEach(function(d) {
            d.year = "2018-2025";
            datas.push(d);
        });
                    
        // Nest by Section
        var nestedByName = d3.nest()
            .key(function(d) { return d.name })
            .entries(datas);
        
        // Calculate yMin
        var y1Min = d3.min(nestedByName, function(d) {
            var v1 = d.values[0].amount;
            var v2 = d.values[1].amount;    
            return Math.min(v1, v2);
        });
        
        // Calculate yMax
        var y1Max = d3.max(nestedByName, function(d) {
            var v1 = d.values[0].amount;
            var v2 = d.values[1].amount;
            return Math.max(v1, v2);
        });
            
        // Calculate y domain for datas
        y1.domain([y1Min, y1Max]);
        
        var yScale = y1;
            
        var voronoi = d3.voronoi()
            .x(d => d.year == "2017" ? 0 : width)
            .y(d => yScale(d.amount))
            .extent([[-margin.left, -margin.top], [width + margin.right, height + margin.bottom]]);
        
        // Axis
        var borderLines = svg.append("g")
            .attr("class", "border-lines")
            borderLines.append("line")
            .attr("x1", 0).attr("y1", 0)
            .attr("x2", 0).attr("y2", config.height);
            borderLines.append("line")
            .attr("x1", width).attr("y1", 0)
            .attr("x2", width).attr("y2", config.height);
        
        var slopeGroups = svg.append("g")
            .selectAll("g")
            .data(nestedByName)
            .enter().append("g")
                .attr("class", "slope-group")
                .attr("id", function(d, i) {
                    d.id = "group" + i;
                    d.values[0].group = this;
                    d.values[1].group = this;
                });
        
        var slopeLines = slopeGroups.append("line")
            .attr("class", "slope-line")
            .attr("x1", 0)
            .attr("y1", function(d) {
                return y1(d.values[0].amount);
            })
            .attr("x2", config.width)
            .attr("y2", function(d) {
                return y1(d.values[1].amount);
            })
            .style("fill", "#69b3a2");
        
        var leftSlopeCircle = slopeGroups.append("circle")
            .attr("r", config.radius)
            .attr("cy", d => y1(d.values[0].amount))
            ;
        
        var leftSlopeLabels = slopeGroups.append("g")
            .attr("class", "slope-label-left")
            .each(function(d) {
                d.xLeftPosition = -config.labelGroupOffset;
                d.yLeftPosition = y1(d.values[0].amount);
            });
        
        leftSlopeLabels.append("text")
            .attr("class", "label-figure")
            .attr("x", d => d.xLeftPosition)
                    .attr("y", d => d.yLeftPosition)
            .attr("dx", -10)
            .attr("dy", 3)
            .attr("text-anchor", "end")
            .text(d => (formatAmount(d.values[0].amount) + "€"));
        /*
        leftSlopeLabels.append("text")
            .attr("x", d => d.xLeftPosition)
                    .attr("y", d => d.yLeftPosition)
            .attr("dx", -config.labelKeyOffset)
            .attr("dy", 3)
            .attr("text-anchor", "end")
            .text(d => d.key);
        */
        var rightSlopeCircle = slopeGroups.append("circle")
            .attr("r", config.radius)
            .attr("cx", config.width)
            .attr("cy", d => y1(d.values[1].amount) + "€");
        
        var rightSlopeLabels = slopeGroups.append("g")
            .attr("class", "slope-label-right")
            .each(function(d) {
                d.xRightPosition = width + config.labelGroupOffset;
                d.yRightPosition = y1(d.values[1].amount);
            });
        
        rightSlopeLabels.append("text")
            .attr("class", "label-figure")
                    .attr("x", d => d.xRightPosition)
                    .attr("y", d => d.yRightPosition)
            .attr("dx", 10)
            .attr("dy", 3)
            .attr("text-anchor", "start")
            .text(d => (formatAmount(d.values[1].amount)));
        
        rightSlopeLabels.append("text")
            .attr("x", d => d.xRightPosition)
            .attr("y", d => d.yRightPosition)
            .attr("dx", config.labelKeyOffset)
            .attr("dy", 3)
            .attr("text-anchor", "start")
            .text(d => d.key);
        
        var titles = svg.append("g")
            .attr("class", "title");
        
        titles.append("text")
            .attr("text-anchor", "end")
            .attr("dx", -10)
            .attr("dy", -margin.top / 2)
            .text(config.leftTitle);
        
        titles.append("text")
            .attr("x", config.width)
            .attr("dx", 10)
            .attr("dy", -margin.top / 2)
            .text(config.rightTitle);

        if(hashExists ()) {
            titles.append("text")
            .attr("x", 0)
            .attr("y", -75)
            .text(decodeURI(location.hash.substr(1)));
        }
        else {
            titles.append("text")
            .attr("x", 0)
            .attr("y", -75)
            .text('Plan pluriannuelle d\'invetsissement');
        }
        
        relax(leftSlopeLabels, "yLeftPosition", config);
        
        leftSlopeLabels.selectAll("text")
            .attr("y", d => d.yLeftPosition);
        
        relax(rightSlopeLabels, "yRightPosition");
        
        rightSlopeLabels.selectAll("text")
            .attr("y", d => d.yRightPosition);
        
        d3.selectAll(".slope-group")
            .attr("opacity", config.unfocusOpacity);
        
        var voronoiGroup = svg.append("g")
            .attr("class", "voronoi");
        
        voronoiGroup.selectAll("path")
            .data(voronoi.polygons(d3.merge(nestedByName.map(d => d.values))))
            .enter().append("path")
            .attr("d", function(d) { return d ? "M" + d.join("L") + "Z" : null; })
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("click", click);
    });

    function mouseover(d) {
        d3.select(d.data.group).attr("opacity", 1);
    
        var mouseVal = d3.mouse(this);
            div.style("display","none");
            div
                .html("<h4>"+d.data.name+"</h4><ul><li>part: "+ Math.round(d.data.pct * 10) / 10 + "%</li><li>"  + d.data.year + ": " + Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(d.data.amount) + "</li></ul>")
                .style("left", (d3.event.pageX+12) + "px")
                .style("top", (d3.event.pageY-10) + "px")
                .style("opacity", 1)
                .style("display","block");
    }
    
    function mouseout(d) {
        d3.selectAll(".slope-group")
        .attr("opacity", config.unfocusOpacity);
    
        div.html(" ").style("display","none");
    }
    
    function click(d) {
        if(!hashExists()) {
            window.location = hrefWithoutHash() + '#' + stripped(d.data.name);
            console.log("#");
        }
        else {
            console.log("/");
            window.location = hrefWithoutHash();
        }
    }
    
    // Function to reposition an array selection of labels (in the y-axis)
    function relax(labels, position) {
        again = false;
        labels.each(function (d, i) {
            a = this;
            da = d3.select(a).datum();
            y1 = da[position];
            labels.each(function (d, j) {
                b = this;
                if (a == b) return;
                db = d3.select(b).datum();
                y2 = db[position];
                deltaY = y1 - y2;
    
                if (Math.abs(deltaY) > config.labelPositioning.spacing) return;
    
                again = true;
                sign = deltaY > 0 ? 1 : -1;
                adjust = sign * config.labelPositioning.alpha;
                da[position] = +y1 + adjust;
                db[position] = +y2 - adjust;
    
                if (again) {
                relax(labels, position);
                }
            })
        })
    }
        
    
    function drawSlopeGraph(cfg, data, yScale, leftYAccessor, rightYAccessor) {
        var slopeGraph = svg.append("g")
        .attr("class", "slope-graph")
        .attr("transform", "translate(" + [cfg.xOffset, cfg.yOffset] + ")");     
    }
    
}

dataviz();