function stripped (str) {
    strip = str.toLowerCase()
        .replace(/ /g,'-')
        .replace(/é/g,'e')
        .replace(/,/g,'-');
    return strip;
}

function hrefWithoutHash () {
    return window.location.pathname;
}

function selectUrl () {
    hash = decodeURI(location.hash.substr(1));
    strippedHash = stripped(hash);

    if(hash=="") {
        return "/data/ppi/ppi.json";
    }
    else {
        return "/data/ppi/" + strippedHash + ".json";
    }
}

function hashExists () {
    hash = decodeURI(location.hash.substr(1));
    strippedHash = stripped(hash);

    if(hash=="") {
        return false;
    }
    return true;
}

window.addEventListener('hashchange', function() {
    this.document.getElementById("slope-svg").remove();
    els = this.document.getElementsByClassName("tooltip");
    Array.prototype.forEach.call(els, function(el) {
       el.remove();
    });
    dataviz();
  }, false);


