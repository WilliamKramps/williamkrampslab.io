Title: Sécurité
Date: 2020-01-29
Slug: sécurité
Status: draft

Vous suivez la campagne des municipales à Clermont?

Je crois que je pourrais la résumer en 4 phrases lapidaires

* @bianchi2020: ce sera vert!
* @jpbrenas: ce sera tranquille!
* @ericFaify: ce sera vert et tranquille!
* Il y a plus de 3 candidats? Vous êtes sûrs?

Alors, c'est vrai, j'ai la critique facile. 

D'aileurs, invariablement, lors de mes nombreux échanges en messages privés avec les co-listiers (vous ne croyez quand même pas que les candidats vont répondre en personne), vient la réflexion: "t'as qu'à te présenter!".

Si je n'étais pas conscient de ce vers quoi nous allons, je pourrais trouver ce retournement assez cocasse. 

Un peu comme si on me proposait d'essayer la dictature, pour me persuader qu'en France on est en démocratie ... 

Pour le politique, sensé représenter les citoyens, tant qu'on ne "fait pas à sa place", on doit se contenter de ce qu'il fait. 

Fort bien.

Je suis du coup allé voir plus loin que le bout de ma circonscription et je me permets de porter à votre attention, vous clermontois.e.s et habitants des alentours, le programme du candidat sortant à Grenoble: Eric Piolle.

J'ai parcouru ces 16 pages avec attention, et elles me paraissent bien plus à la hauteur de [l'urgence climatique déclarée par Olivier Bianchi en septembre dernier](https://www.lamontagne.fr/clermont-ferrand-63000/actualites/clermont-ferrand-vote-l-urgence-climatique-opportunisme-ou-acceleration-ecologique_13651064/) que le volet écologique du programme [Bianchi2020](https://olivierbianchi2020.fr/wp-content/uploads/2020/01/Programme2020_vDEF.pdf), ou celui d'[Eric Faidy](https://ericfaidy.fr/https://ericfaidy.fr/), ou encore celui de [Jean Pierre Brenas](https://www.jeanpierrebrenas.com/) ... 

Pour les autres candidats j'attends qu'ils mettent leurs propositions en ligne pour les commenter.

Je vous propose les quelques réflexions que je me suis faites en lisant ce programme.

### Surprise: il n'y a pas de volet vert dans ce programme!

Bas de budget Carbone, Pas de conseil citoyen de l'environnement, pas de bonus ou de malus CO2 par projets, pas d'ajoint à l'environnement. 

Etonnant non?

C'est simplement que le programme est entièrement centrée sur l'urgence écologique.

Et oui l'urgence climatique, au delà de son érosion médiatique, a un sens: il y a URGENCE!

il faut donc en faire LA PRIORITE!

on ne négocie pas avec la priorité, c'est LA PRIORITE ... 

Pour le moment, à Clermont, c'est une composante au même titre que la tranquilité publique, la culture ou encore l'économie: un petit pour Pierre, un petit poour Jacques :D surtout si Pierre et Jacques ont tous les deux une chacne de voter pour moi.

### Le courage du constat

Je cite: "Nous savons, aujourd’hui, notre société particulièrement vulnérable à des crises d’une nature et d’une ampleur
inédite et mal connue... Il ne s’agit pas là d’une vision prospective abstraite, mais bien d’un futur engagé, dont les premiers signes sont déjà présents dans notre quotidien (dégradation de l’environnement, climat social, etc.)"

Déclarer l'urgence climatique, quand Greta Thunberg est à la une des journaux, ça s'appelle suivre la mode.

Traduire cette urgence, et la partager avec ses administrés, sans paniquer, ça s'appelle faire preuve de clairvoyance, de courage et d'honnêteté!

## La clareté de l'objectif

Je cite toujours:

* "Il s’agit alors de chercher à développer notre résilience, face aux crises à venir.". 
* "Les efforts pour développer la résilience ne sont pas désirables, mais nécessaires, en lucidité."
* "Ces efforts passeront par l’actualisation des solidarités, des échanges et des interconnexions possibles depuis l’échelon communal"

La réslience est un projet fédérateur par essence, et une bonne occasion de revisiter le vivre ensemble sur notre territoire. 

Promouvoir, stimuler, communiquer pour mobiliser, réfléchir et faire ensemble.

C'est sûr, c'est un peu plus compliqué à vendre aux électeurs, que les caméras de vidéosurveillances et l'armement de la police municipales.

## Des Communs

Vocable désespérement absent des programmes que j'ai lu jusque là pour Clermont Ferrand. Même si au moins 2 listes, dont je n'ai toujours pas lu les propositions, par leur nom, laisse présager une ouverture.

Dommage. Vraiment dommange!

C'est bel et bien autour des Communs que se joue la résilience et le projet commun, justement.

Alors on peut le voir comme un mot valise qui fait suite à tant d'autres, la transition, le développement, le durable.
Mais on peut aussi s'intéresser à la définition qu'en a proposé Elinor Ostrom (Prix Nobel d'économie), ou considérer la façon dont en parle Gaël Giraud (["Construire un monde en commun ? Les communs comme projet politique"](https://www.youtube.com/watch?v=L2JR-r7Elds)).

Une fois qu'on a compris ce que c'était qu'un commun, on peut commencer à les lister: les communs positifs et les communs négatifs.

On peut ensuite faire l'inventaire, de leurs vulnérabilités, de leurs potentiels, des acteurs qui les exploitent, des acteurs qui les consomment, du savoir dont on dispose sur eux, de leurs résiliences et finalement prendre des décisions avisés pour ces Communs et les acteurs qui y sont associés. 

Inutile d'appeler un cabinet d'expert pour ça hein?

Toutes les forces pour se travaille sont déjà en présence: des chercheurs, des associations, des entreprises. 

Et toute celles que j'ai croisé ne demande qu'à participer.

## Mener des expérimentations

l'innovation à Grenoble ce n'est pas qu'une "fiscalité avantageuse pour l'installation d'entreprises innovantes".

Si je n'ai aucune illusion sur ce que peuvent penser les candidats @EricFaidy et @JPBrenas d'une ZAD ou d'une ZAT, je trouve regrettable que le candidat Bianchi brosse les entreprises dans le sens du poil, et qu'il oublie de valoriser quelques projets qui ont été soutenues (à quel point peut être est ce là la question?) par la municiaplité pendant ce mandat:

* la coop des domes
* la ferme urbaine
* le Parc Oasis
* le repair café
* ... (merci de compléter cette liste de projets en lien avec la résilience qui ne se prétend pas du tout exhaustive).

Dommage. 
Manque d'intérêt? 
Manque d'ambition? 
Pour le territoire c'est certain. 
Pour une carrière politique: peut être pas.

## L'échelle

Toutes les propositions de Grenoble ne commun concerne la métropole.

Seule Clermont n'est rien. Sa résilience réside dans le maillage et les interactions des communes voisines. 

C'est de la métropole que doit partir l'impulsion car c'est à ce niveau là que sont toutes les compétences https://www.clermontmetropole.eu/fr/ma-metropole/linstitution-a-la-loupe/competences/. Et très vite il faut s'étendre à la [Communauté d'agglomération Riom Limagne et Volcans](https://fr.wikipedia.org/wiki/Communaut%C3%A9_d%27agglom%C3%A9ration_Riom_Limagne_et_Volcans) à la communauté [Mond'Arverne](https://www.mond-arverne.fr/) à la [communauté de commune des massifs du sancy](http://www.cc-massifdusancy.fr/).

Je n'ai vu aucun candidat chez nous appeler à un tel élan. 

Encore dommage

## Des propositions

OUI tout les programmes des candidats clermontiois ont des propositions communes avec Grenoble, le permis de végétaliser, les ilôts de fraîcheur ...

MAIS d'une part Grenoble en commun va bien plus loin que n'importe lequel de ces 3 programmes.

D'autres part, ces propositions ne sont qu'un patchwork d'engagement de campagne si elles ne sont pas articulées autour du projet de résilience ... 

Pas de projet commun, pas de vision globale du territoire, pas de prise de conscience, pas de gestion du risque ...

## Conclusion

Il résulte des ces rendez vou rater avec l'ambition, et peut être bien avec les électeurs, un consensus mou cherchant désepérément à ménager la chèvre te le chou, le populo et le CSP+, le bobo et le prolo, le jeune et le boomer ...

Le pire dans tout ça, c'est que nous sommes nombreux à savoir que la résilience de notre territoire est aussi au coeur des services de prospectives de nos grands acteurs locaux: Michelin, Limagrain (qui comme le reste des exploitants agricoles enregistre une baisse de 40% de sa production), et bien d'autres. 

Car la sécurité des clermontois, c'est celles de leurs employés!

Dis autrement, je ne souhaite pas trop de secousses au prochain maire pour le prochain mandat, car il risquerait bien vite d'avoir à gérer des dossiers bien plus compliqués et bien plus impliquant qu'un mirroir d'eau aux salins, une aire d'autoroute dédiée à l'environnement (mais LOL quoi!), une extension de stade de foot, ou un  dossier Clermont Capitale Européenne de la culture ...

Depuis 2 mois que je suis cette campagne j'ai compris 3 choses:

* si ce n'est pas nous les clermontois.e.s et habitants des alentours qui nous emparons de l'urgence, il ne se passera rien.
* prendre en compte l'urgence est bel et bien possible puisque d'autres villes le font
* il n'y a plus une seconde à perdre

Je lance donc ici un appel à tout ceux qui pensent que les propositions des ces municipales ne sont pas à la hauteur de l'urgence: libérez votre parole, et éveillez [doucement](/parler-effondrement-resilience.html) les consciences, regroupons nous et réfléchissons ensemble à la démarche qui nous permettra de réunir les expertises, la légitimité et le pouvoir de peser.

Il faut ensemble se me mettre au travail afin d'organiser dés maintenant la résilience qui n'a aucune chance de venir de nos élus.