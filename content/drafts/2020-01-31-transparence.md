Title: Transparence
Date: 2020-02-02
Slug: transparence
Status: draft


bien que la France soit dans une dynamique d'[ouverture des données publiques](https://www.gouvernement.fr/action/l-ouverture-des-donnees-publiques) il est souvent compliqué de trouver des données précises et suivies.


Je pense que la transparence entre les administrés et les élus est particulièrement importante au niveau local, car elle est un moteur de confiance.

Les sevrices de la municipalité et de la métropole devraient rendre des comptes sous forme de données brutes tout au long de son mandat.

## des données brutes c'est quoi?

C'est la mise à disposition de données éxhaustives, et structurée permet à chacun de se rendre compte, et à chaun de faire ses propres calculs.

Comprenons nous, je ne dis pas que la municipalité est opaque. 

existe de vrais outils de communicaiton entre la municipalité / la métropole et ses administrés.
- [Demain Clermont](https://fr.calameo.com/books/00000020925e6c676c504) 
- [Métropole](https://www.clermontmetropole.eu/outils-pratiques/publications/) 
- [Service des eaux](http://www.services.eaufrance.fr/donnees/collectivite/46559)
- [Rapport annuel 2018 / T2C](https://fr.calameo.com/read/0002507364387b7b37acc?page=1)
- [Budget 2018](https://clermont-ferrand.fr/docs/delib/CM21122017/21%2012%202017_ODJ03.pdf)
- [Budget 2019](https://clermont-ferrand.fr/docs/delib/CM18122018/18+12+18_ODJ05.pdf)

Mais ce sont des rapport avec éventuellement des données, mais souvent trop agrégés.

Un accès aux données brutes permet de faire soi même ses propres analyses, d'alimenter le débat, tant pour les journalistes locaux, que pour les administrés.

Un bon exemple de ce que sont des données brutes serait 

- [Les données sur la qualité de l'air à Clermont Ferrand mises à disposition par atmo](https://www.atmo-auvergnerhonealpes.fr/donnees-ouvertes-de-qualite-de-lair-0)

Un bon exemple de data journalisme, c'est à dire de présentation des données burtes serait

* [Budget municipal de Clermont-Ferrand (63000) par le JDN](http://www.journaldunet.com/business/budget-ville/clermont-ferrand/ville-63113/budget)

C'est un moyen d'appuyer des points de vue et des prises de décisions en présentant des arguments chiffrés et donc plus objectifs.

Ce serait également un moyen objectif de présenter le bilan du prochain mandat, en regroupant toutes les données brutes et en proposant des analyses reproductibles aux administrés.

A mon avis il manque (n'hésitez pas à compléter cette liiste):

- des données sur la sécurité à clermont [les dernières données de l'ONDPR datent de 2014](https://www.data.gouv.fr/fr/organizations/observatoire-national-de-la-delinquance-et-des-reponses-penales-ondrp/)
- des données sur la biodiversité
- des données sur l'urbanisme et les esapces verts 

## Transparence sur les objectifs

La petite enfance était la prioirté du mandat 2014-2020 à Clermont Ferrand. 
Force est de constater que de nombreux efforts et amélirations ont été consentis sur ce point.
Si tous les projets ne peuvent pas être annoncés à l'avance, les voir la, grande priorité du prochain mandat devrait être claire, des objectifs à atteindre desvraient être présentés, et des indcateurs de réalisations devraient être mis à disposition de tous.

[Quand Quarante-six députés de tous bords appellent à élargir l’étude d’impact climatique](http://www.acceleronslatransition.fr/quarante-six-deputes-de-tous-bords-appellent-a-elargir-letude-dimpact-climatique/), je pense que nos candidats devraient s'engager à ce que l'urgence climatique, la prise en compte de l'[effondrement](/pages/effondrement.html) à tous les niveaux, et l'objectif de la [résilience](/pages/resilience.html) soient la priorité du prochain mandat.

Il devraient également être proactif sur la proposition d'objectifs à atteindre, et d'indicateurs de réalisations de ces objectifs.

