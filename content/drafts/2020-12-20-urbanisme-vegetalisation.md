Title: Urbanisme et végétalisation
Date: 2019-12-20
Slug: urbanisme-vegetalisation
Status: draft


## POUR UNE VILLE NATURE
https://www.jeanpierrebrenas.com/une-ville-respirable-et-agreable-a-vivre-2


https://www.lamontagne.fr/clermont-ferrand-63000/actualites/trois-raisons-qui-expliquent-le-boom-des-constructions-immobilieres-a-clermont-ferrand_13551798/

https://www.lamontagne.fr/clermont-ferrand-63000/actualites/un-nouveau-parc-en-2026-a-clermont-ferrand-avec-la-tiretaine-au-milieu_13632448/#refresh

## Grands projets

* St Jean 
    * Kandinsky par l'agence Reichen et Robet (prête le flanc dommange)
    *  le plan guide, soit le seul outil capable de garantir la cohérence du projet dans le temps. Il est amené à évoluer du fil des années. Ca marche comment?
    * le vert est déplacé?
    * combien: l'agence? le projet? 40 hectares / pour combien de verdure

* Cataroux
    * Promoteur Quartus 60M e
    * parking relais +1
    * réno plutôt que tout neuf +1
    * plan guide

* https://www.lamontagne.fr/clermont-ferrand-63000/actualites/un-nouveau-parc-en-2026-a-clermont-ferrand-avec-la-tiretaine-au-milieu_13632448/

## Qualité de l'air

* Nicolas Vigier, responsable de l'unité prévision d'Atmo Auvergne-Rhône-Alpes
* Cyril Besseyre, le référent territorial Auvergne
* Nicolas Bonnet, le président du comité territorial Auvergne.