Title: Aidez les Verts! Votes Clermont en Commun
Date: 2020-03-10
Slug: aidez-les-verts-votez-clermont-en-commun
Status: draft

## Aidez les Verts! Votez Clermont en Commun

Je vais le dire autrement

## Aidez Nicolas Bonnet! Votez Floren Naranjo

Je m'explique. Je me passe depuis plusieurs semaine cet extrait du conseil métropolitain du 14 février dernier...
Et je voudrais en partager avec vous ce que cela m'inspire.

D'abord un peu de contexte, on est au conseil métropolitain, composé [des conseillers métropolitains des 19 communes qui composent la métropole](https://fr.wikipedia.org/wiki/Clermont_Auvergne_M%C3%A9tropole#Les_.C3.A9lus).

L'exercice est le suivant votez pour ou contre des décisions que la métropole doit prendre sur [tous les sujets qui concerne ses compétences](https://fr.wikipedia.org/wiki/Clermont_Auvergne_M%C3%A9tropole#Comp.C3.A9tences).

Ces décision ont été présentées et logiquement discutées au sein de commissions au préalable. Chaque conseiller est donc sensé être informé sur la décision à prendre et peut ainsi voter en son âme et conscience lorsque la question est posée au conseil métropolitatin.

Quelques remarques sur l'exercice:

- comme vous pouvez le constater c'est long, ça dure 3h49 ce coup là ... mais ç apeut être encore plus long.
- pour se donner une idée du volume des questions traitées je vous invite à consulter ici [le compte rendu des délibérations du conseil métropolitain du 14 février 2020](https://www.clermontmetropole.eu/ma-metropole/linstitution-a-la-loupe/deliberations/): une centaine de question. Toutes les infos sont publiées ... une fois que les décisions ont été prises ;)
- chaque question est lapidaire: une phrase! elle fait écho à des dossiers parfois complexes, et ou volumineux. Si vous prenez la peine de discuter avec des conseillers métropolitains consciencieux, vous apprendrez plus sieurs choses intéressantes:
    - certaines commissions sont publiques c'est à dire que le quidam peut prétendre à connaître les informations avant le vote ... CA je ne sais pas comment on peuy s'y prendre pour y accéder. Mais je suis un citoyen apprenant donc n'hésitez pas à me proposer des marches à suivres.
    - les commissions sont parfois qualifiées par les conseillers de "chambres de'enregistrement", c'est à dire qu'ils considèrent qu'au moment de la commission la décision a déjà été prise et que les informations et surtout la question posée ne permet pas d'influer réellement sur l'exécution ou non du projet
    - un conseiller recoit, 5 à 7 jours avant le conseil municipal toutes les informations par la poste format papier + CD ROM (si si!). Certains dossiers sont volontairement abscon, voire sur volumineux pour noyer le poisson.

Bref!

L'extrait que je vous propose concerne UNE question: "[COURNON D'AUVERGNE : ACQUISITION À L'AMIABLE - PDS SARLIÈVE SUD](https://www.clermontmetropole.eu/fileadmin/user_upload/Conseils_communautaires/2020_02_14_-_Conseil_metropolitain_du_14_fevrier_2020/Urbanisme_-_Espaces_Naturels_-_Cours_D%27eau_-_Tourisme/DEL20200214_062.pdf)"




La vidéo est là (y en a )