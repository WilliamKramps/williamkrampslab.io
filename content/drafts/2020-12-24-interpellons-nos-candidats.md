Title: Interpellons nos candidats
Date: 2020-01-24
Slug: interpellons-nos-candidats
Status: draft

Si vous êtes d'accord avec mon point de vue, au moins en partie, et que vous aussi vous pensez qu'il y a urgence pour le climat,pour la prise en compte d'un [effondrement](/pages/effondrement.html) probable et fixer la [résilience](/pages/resilience.html) comme une priorité du prochain mandat, c'est le moment ou jamais de faire les bons choix pour **élections Municipales et communautaires** des 15 et 22 mars 2020 à [Clermont Ferrand](https://clermont-ferrand.fr/) / [Clermont Métropole](https://www.clermontmetropole.eu/ma-metropole/la-carte-et-le-territoire/que-fait-la-metropole/).

Alors que vous soyez un simple administré, ou un militant associatif, je vous invite à écrire à tous les candidats en leur soumettant vos points de vues et les engagements que vous voulez qu'ils prennent.

Je me propose de recenser vos interpellations sous tous les formats (image, fichier doc ou PDF) et les réponses éventuelles qui y seront apportées. [Envoyez les moi pour que je les liste sur cette page](mailto:williamkramps@protonmail.com).


**N.B** n'hésitez pas à user des vertus du [pseudonymat](/pages/pseudonymat.html) ;)