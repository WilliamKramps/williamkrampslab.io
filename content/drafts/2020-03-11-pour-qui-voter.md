Title: Pour qui voter?
Date: 2020-03-11
Slug: pour-qui-voter
Status: draft

Tout d'abord, je vous confirme que l'expérience du [pseudonymat](/pseudonymat.html) est vraiment intéressante dans une élection comme les municipales. 

J'ai pu tout dire, même des conneries, et j'ai pu les assumer sans me poser trop de questions ... Avoir une liberté de ton ça change ce que l'on dit c'est certain.

J'ai échangé avec TOUS les candidats soit par les réseaux sociaux, soit en présence (sauf Marie Savre de LO car je n'ai aucun contact).
J'ai eu globalement des échanges plutôt cordiaux dans la majorité des cas, et même si je me suis fâché quelques fois j'ai trouvé les gens relativement honnête intellectuellement. (si si si je le pense).

Je vais vous communiquer mon opinion à date sur les Municipales à Clemront Ferrand (encore une chose qu je ne ferai pas si je n'étais pas sous pseudonymat). Mes apppréciations sont conditionnées par la proccupation de l'[effondrement](/pages/effondrement.html) à venir et de la [résilience](/pages/effondrement.html) qui devra se mettre en place le plus rapidement possible.

Je reste tout de même persuadé que sensbiles ou non à ces deux sujet nous avons tout à gagner à s'organiser sur notre territoire, ivi et maintenant.

Mon approche s'est beaucoup basée sur les propositions que j'ai pu lire notamment sur les sites web des candidats. 

https://www.epicentrefactory.com/2020/03/cinq-candidats-face-a-la-transition-ecologique/

Je commencerai donc par ceux que je n'ai pas lu, par ce que je ne les ai pas trouvé: 

## Anne Biscos - RN

Je n'ai pas vu de tracts, je n'ai pas vu de site web. 
J'ai échangé une fois en DM avec @annebiscos.
L'échange n'était pas désagréable mais je n'ai rien à en dire en fait.

## Jean Pierre Brenas (JPB) - LR

J'ai lu, j'ai vu et je crois avoir compris.

JPB est très préoccupé par l'entreprenariat, le développement, le progrés et la sécurité.

Il incarne parfaitement le "solutionnisme": qui brandit systématiquement les technologies comme solution à n'importe quel problème.

Avec les "data" et le "digital" JP nous a appris qu'on pouvait régler 
* la pollution avec des capteurs pour les particules fines
* l'imperméabilité des sols avec des capteurs d'humidité
* les menus de la cantine avec un logiciel et bien plus encore ...

J'ai été assez surpris du manque de maîtrise de certains dossiers, notamment celui de l'eau. Je ne suis vraiment pas en confiance quand il dit qu'on pompe dans l'Allier alors qu'on pourrait boire l'"eau des puys". Sachant que l'"eau des puys" représente péniblement 20% de nos ressources hydriques au niveau de Clermont Ferrand (et encore c'est 0% de mai à octobre). 

J'ai l'impression que JP n'est pas tout à fait prêt pour l'effondrement.

JPB est pour soutenir le tissu associatif ... le temps que l'association devienne une entreprise.

Il n'aime pas les punks à chiens et les gens qui pissent dans les impasses.

## Eric Faidy

Eric Faidy m'a beaucoup intrigué: partir en marche, pour être le premier maire écolo et redonner le pouvoir au clermontois, ca me paraissait être un peu comme regarder 

## La Team Bianchi

## Bianchi

## Clermont en commun 

## Cause commue

Je rappelle que tous les liens des candidats sont ici car tous ne sont pas trouvable à partir d'un moteur de recherche