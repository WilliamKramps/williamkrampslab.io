Title: Pain au levain
Date: 2020-04-20
Slug: pain-levain
Status: draft

Si vous avez du temps faites du pain! 

Comme c'est le bon moment pour enclencher le mode DIY on va faire du pain au levain et on va commencer par faire son levain

3 considérations préalables valables pour ala confection du levain et du pain:

1 - je vous conseille de tirer l'eau la veille dans un pichet et de la laisser reposer 12 à 24h pour que le chlore puisse s'évaporer.
2 - une source de chaleur douce pour faire lever le levain et la pâte est un vrai plus, il faut que la température soit idéalement entre 20-40°C au délà les levures sont ralentis voire détruites. Pour obtenir cette source de chaleur, vous pouvez utiliser 
    - un four à 35°C en attendant un peu après qu'il ait chauffé
    - une casserole d'eau bouillante dans le four (vous aurez de l'humidité si l'eau est bien bouillante je vous laisse gérer)
    - une box internet, un disque dur, une alimenation électrique sur laquelle poser le contenant
    - le soleil en recouvrant le contenant
3 - la balance électronique est un vrai plus

## Levain

### Naissance

Il vous faut 

- de la farine de seigle (ça facilite bien les choses)
- un pot à confiture vide et son couvercle
- une petite cuillère avec un manche un peu long (genre cuillètre à thé)
- de l'eau qui a réposée comme expliqué ci-dessus
- un source de chaleur douce expliqué ci-dessus
- de l'huile de coude 

Le levain, c'est comme Rome ça ne se fait pas en un jour. 

Pendant 5 jous vous allez répéter le petit rituel suivant matin et soir

1 - enlever la moitié de ce qu'il y a dans le pot à confiture (sauf si c'est la première fois évidemment)
2 - verser 25g de farine de seigle
3 - verser 25g d'eau
4 - touiller énergiquement en mélangeant bien avec le fond du pot
5 - faire reposer le tout prêt d'une source de chaleur douce, avec le couvercle juste posé (pas vissé)

Au bout de 4 à 5 jours (ne pas désespérer) votre mixture devrait tripler de volume: votre levain est né.
Donnez lui un nom ... je vous suggère William.

Attention il y a parfois un faux départ à 2 jours, ne vous laissez pas berner et continuer consciencieusement le petit rituel.

### Entretien

Il faut nourrir son levain avant de l'utiliser, c'est à dire ajouter 25g de farine de seigle et 25g d'eau et touiller énergiquement, on dit aussi qu'on l'active. 

Si vous utilisez votre levain tous les jours ou tous les deux jours: nourrissez le 2 à 4 heures avant l'utilisation et laissez le couvercle posé (pas vissé) sur votre plan de travail.

Si vous utilisez votre levain moins souvent: mettez le au frigo et sortez le 2 heures et nourrissez le 2 à 4 heures avant l'utilisation. 

2 à 4 heures après l'avoir nourri (selon la chaleur) votre levain va tripler de volume, il est alors prêt à l'emploi.

ok pour le levain? 

C'est magique! 

Et ci vous le soignez bien il peut durer INDEFINIMENT!

## Le Pain

Il y a 1001 techniques et recettes, voici celle que je pratique depuis le début du confinement.

Ne vous découragez pas et décomplexez-vous: vos premiers pains seront moches! Mais toujours mangeables, du coup on ne perd jamais complètement sont temps. 

Il vous faut:

- un saladier
- une cocotte minute avec soit une poignée de couvercle en plastique dévissable (que vous remplacerez par un bout d'alu), soit une poignée de couvercle en métal qui passe au four
- un four qui chauffe à 240°C
- une grande cuillère
- une lame de rasoir (on appelle ça la grigne)
- 500g de farine T80 (ça marche mieux qu'avec de la T65)
- quelques grammes de farine supplémentaires pour fariner le plan de travail et le saladier
- 7g de sel
- 320g d'eau qui a réposé comme expliqué ci-dessus
- un source de chaleur douce comme expliqué ci-dessus

Les grandes étapes:

1 - mélanger 50g à 80g de levain, 150g de farine (T80 donc) et 320g d'eau. Touiller bien, si  les grumeaux rapliquent, utilisez un fouet?
2 - laisser reposer 1 à 2 heures.
3 - ajouter les 7g de sel et les 350g de farine T80. Commencer à mélanger avec la grande cuillère. Quand vous ne pouvez plus tourner la cuillère, mettez y les mains (farinées parce que ça va coller). Pétrissez: vous devez obtenir une boule de pâte (on appelle ça un pâton). farinez votre saladier déposez y le paton.
4 - laisser reposer 30 minutes à 2 heures

5 - faire faire 4 **tours** à la pâte pendant 1 minutes, à 10 minutes d'intervalles. Le but d'un tour (on appelle cette étape également "stretch and fold" - étirer et plier) est d'étirer le réseau de gluten qui est en train de se former dans votre pâte et d'y faire rentrer dans l'air. Il faut donc étirer votre pâte sans la déchirer et la replier, dans un sens, puis dans l'autre comme un portefeuille. A la fin de chaque séquence faites une boule 

6 - faites reposer 4 à 6 heures le paton à côté d'une source de chaleur douce
7 - donner la forme à son pain, on appelle cette étape le façonnage. Selon la forme que vous souhaitez donner, cette vidéo donne des techniques vraiment pas mal. 
https://www.youtube.com/watch?v=WdROz5L2n5Y
Lui il dit de dégazer la pâte, personnellement je ne le fais pas
8 - une fois façonné, vous pouvez laisser reposer le paton pour qu'il gonfle un peu, personnellement je n'attends pas très longtemps à cette étape (15 à 30 minutes).
9 - Préchauffer le four à 240 avec la cocotte et le couvercle à l'intérieur. L'idée est que le paton prenne un choc thermique. Donc j'enchaine les étapes suivantes assez rapidement:

    - sortir la cocotte du four et enlever le couvercle
    - mettre le paton à l'intérieur et lui mettre quelques coups de rasoirs, on appelle ça grigner. De ce que j'ai pu expérimenter une balafre dans tous le sens de la longueur est assez efficace. Deux coups de rasoirs supplémentaires sur un côté peuvent aussi donner de bons résultats. Inutile de faire des entailles trop profonds 3 ou 5 mm suffisent. Jetez un peu d'eau (l'équivalent d'une ou deux cuillères à soupe) sur le paton, remettez le couvercle et réenfournez
    
10 - laissez cuire 30 minutes à 240°
11 - ensuite sortez la cocotte, enlevez le couvercle et réenfrounez

Voilà! si votre pain colle à la cocotte, n'insistez pas et laisser le refroidir, il se décollera tout seul.