Title: Le pari de l'effondrement et de la résilience
Date: 2020-01-22
Slug: pari-effondrement-resilience
Status: draft

J'aime bien parler. Avec des gens. Echanger des points de vue.

Il m'arrive régulièrement d'échanger avec mes interlocuteurs sur les futurs probables qui se dessinent.

Si le [capitalisme de surveillance](https://www.monde-diplomatique.fr/2019/01/ZUBOFF/59443) me paraît avoir encore quelques années devant lui, l'[effondrement systémique](/pages/effondrement.html) me semble un scénario de plus en plus plausible à moyen terme.

Je ne vais pas revenir sur les différents réactions quand on [parle effondrement et résilience](/parler-effondrement.html), je voudrais plutôt ici, présenter 3 approches qui me paraissent rationnelles en 2020 et qui me semblent plaider pour la prise en compte des risques des crises à venir.

## Approche "philosophique"

Blaise Pascal que nous connaissons tous en tant que clermontois a écrit un texte majeur appelé Le [pari de Pascal](https://fr.wikipedia.org/wiki/Pari_de_Pascal), dans lequel il tente de prouver qu'une personne rationnelle a tout intérêt à croire en Dieu, que Dieu existe ou non.

En gros celui qui croit ne se ferme aucune porte, car si Dieu existe il ira au Paradis, et si Dieu n'existe pas, il se retrouvera dans la même situation que celui qui ne croit pas.

Cet argument me paraît complètement transposable à l'[effondrement systémique](/pages/effondrement.html): 

- Qu'avons nous à perdre à considérer le risque d'[effondrement](/pages/effondrement.html)? Selon l'adage on est jamais trop prévoyant: Rien!
- Qu'avons nous à gagner à améliorer la [résilience](/pages/resilience.html) de notre ville? Plus de sécurité c'est sûr, mais aussi une occasion de repenser l'organisation de notre territoire dans une perspective d'autonomie et de relocalisation !

## Approche "Gestion du risque"

Depuis la [catastrophe de Tchernobyl](https://fr.wikipedia.org/wiki/Catastrophe_nucl%C3%A9aire_de_Tchernobyl) nous sommes entrés d'après [Ulrich Beck](http://www.librairielesvolcans.com/9782081218888-la-societe-du-risque-ulrich-beck/) dans la "société du risques".


En 2020 nous passons tous beaucoup de temps à gérer des risques:
- en souscrivant à des assurances
- en mettant en place de nouvelles normes ou règlementation
- en inventant de nouvelles technologies
- etc ...

Nous avons théorisé la [gestion des risques](https://fr.wikipedia.org/wiki/Gestion_des_risques) jsuqu'à en faire [une norme à part entière](https://www.iso.org/fr/standard/35683.html). 

En gros un risque est caractérisé par:
- un événement redoutée: tremblement de terre, rupture d'approvisionnement en carburant, 
- une probabilité d'occurrence:
- un impact


Cette culture du risque est à mon avis aussi bénéfique, qu'handicapante.

En n'identifiant pas les "bons risques" on peut facilement passer à côté de l'essentiel, et passer du temps et de l'énergie à gérer un risque mineur par rapport à une 

Et c'est mon point ici!

Les collectivités terriitoriales n'échappent pas à la gestion du risques, elles sont un document que l'on nomme [DICRIM](http://www.puy-de-dome.gouv.fr/document-d-information-communal-sur-les-risques-r1144.html), pour Clermont se document a fait l'objet d'une [synthèse à destination des puy dômois en 2012](https://fr.calameo.com/read/000000209b0e8c2ea0a41).

En gros pour constituer un tel document on fait une [analyse de risques](https://fr.wikipedia.org/wiki/Analyse_des_risques). ce qui consiste essentiellement à identifier des scénarios redoutés, à évaluer leur probabilté d'occurrence (les 'chances qu'ils arrivent effectivement) et leur impact repectifs.

Pour les puy de domois [Document d’Information Communal sur les Risques Majeurs (DICRIM)](http://www.puy-de-dome.gouv.fr/document-d-information-communal-sur-les-risques-r1144.html) les risques suivant son pris en compte:

* risque inondation
* risque transport e matières dangereuses
* risque mouvement de terrain / effondrement (pas le même que l'[effondrement](/pages/effondrement.html) dont je parle)
* risques météorologiques
* risque sismique
* risques sanitaires

Personnellement je pense que l'[effondrement systémique](/pages/effondrement.html) est un risque suffisamment probable, et impactant pour figurer dans ce document ... [et je suis loin d'être le seul](https://sosmaires.org/lettre-aux-maires/)

Les impacts sur la population sont à prendre en compte mais aussi, les impacts sur la biodiversité, sur l'économie, sur la finance, sur la sécurité etc ...

Peut être qu'on me répondra que dans le cas d'un [effondrement systémique](/pages/effondrement.html), l'état nous viendra en aide ... 

je pense que miser sur une réponse locale que nous avons élaboré, est plus sûr que de miser sur une solution éventuelle qui viendrait "d'en haut"

Dit autrement: si on ne le fait pas personne ne le fera pour nous.

## Approche  par le réenchantement

Comment transformer notre fatigue du discours apocalyptique sur le réchauffement climatique en action pour le résoudre
https://www.ted.com/talks/per_espen_stoknes_how_to_transform_apocalypse_fatigue_into_action_on_global_warming?language=fr#t-157546

On parle donc bien de la même chose à ce moment là. 

Alors pourquoi prétendre qu'on y croit pas, si on y a déjà réfléchi?

Il y a une vrai raison à mon avis: plus on y réfléchit, plus on panique!

L'[effondrement](/pages/effondrement.html) est profondément anxiogène, ça ne vous aura pas échapper, et plus vous passez de temps à l'anticiper plus vous ressassez des scénarios qui ne font vraiment pas enive, avec en plus l'impression de raisonner dans le vide, dans la mesure où ... on ne sait pas ce qui se passera!!!

Il y a à mon avis deux approches rationnelles sur ce problème, une approche "gestion du risque" et une approche "philosophique"

